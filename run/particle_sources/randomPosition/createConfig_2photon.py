# script to create a particle source file
import random
from random import seed
from optparse import OptionParser

parser = OptionParser()
parser.add_option('-d', '--distance', help='--distance 0p2', type=str, default='0p2') # distance between the photons
(options, args) = parser.parse_args()
distance = options.distance
#seed(1)
distance = distance.replace("p",".")
#randomX = random.uniform(0, 66.36)
#randomY = random.uniform(0, 84.98)
randomX = random.uniform(0, 66.36*2)
randomY = random.uniform(0, 84.98*2)
print("### source 1 ####")
print("/gps/particle gamma")
print("/gps/pos/type Plane")
print("/gps/pos/shape Circle")
print("/gps/pos/centre %.4g %.4g -40 mm" % (randomX,randomY))
print("/gps/pos/radius "+str(distance)+" mm")
print("/gps/direction 0 0 1")
print("/gps/ene/type User")
print("/gps/hist/type energy")
print("/gps/hist/point 0 0")
print("/gps/hist/point 100000 0")
print("/gps/hist/point 3500000 1\n")

print("### source 2 ####")
print("/gps/source/add 1")
print("/gps/particle gamma")
print("/gps/pos/type Plane")
print("/gps/pos/shape Circle")
print("/gps/pos/centre %.4g %.4g -40 mm" % (randomX,randomY))
print("/gps/pos/radius "+str(distance)+" mm")
print("/gps/direction 0 0 1")
print("/gps/ene/type User")
print("/gps/hist/type energy")
print("/gps/hist/point 0 0")
print("/gps/hist/point 100000 0")
print("/gps/hist/point 3500000 1")
print("/gps/source/multiplevertex 1")

