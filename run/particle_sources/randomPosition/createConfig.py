# script to create a particle source file
import random
from random import seed
from optparse import OptionParser

parser = OptionParser()
parser.add_option('-d', '--distance', help='--distance 0p2', type=str, default='0p2') # distance between the photons
parser.add_option('-n', '--nPhotons', help='--nPhotons 1', type=str, default='1') # number of photons
parser.add_option('-s', '--seed', help='--seed 1', type=int, default=1) # random seed
parser.add_option('-e', '--energy', help='--energy 1000', type=str, default='1000') # energy of the photon in GeV
parser.add_option('--straight', help='straight photons',default=False, action='store_true')
(options, args) = parser.parse_args()
distance = options.distance
nPhotons = options.nPhotons
iseed = options.seed
energy = options.energy
straight = options.straight

if not straight:
    print('# shooting with an angle')
seed(iseed)
distance = distance.replace("p",".")
distance=float(distance)*0.5 # divide distance over two since below we are defining a radius and not a diameter
# sensitive area size
x_size_over2 = 132.72/2. # mm 
y_size_over2 = 169.96/2. # mm
# FASER magnet aperture is 20cm
magnet_aperture_over2 = 200./2. # mm 
randomX = random.uniform(0, x_size_over2*2)
randomY = random.uniform(0, y_size_over2*2)
# 'decay' particle anywhere around -3.5m and 0m
# the detector starts at -tungsten_thickess/2.
tungsten_z = 3.5
detector_z0 = -tungsten_z/2.
randomZ = random.uniform(detector_z0,-3500) # 3.5m of decay volume

# 1 photon
if (nPhotons == '1'):
    print("/gps/particle gamma")
    print("/gps/pos/type Plane")
    print("/gps/pos/shape Rectangle")
    print("/gps/pos/centre %.4g %.4g %.4g mm" % (x_size_over2,y_size_over2,randomZ))
    print("/gps/pos/halfx %.4g mm" % (x_size_over2))
    print("/gps/pos/halfy %.4g mm" % (y_size_over2))
    print("/gps/direction 0 0 1")
    if energy == 'random':
        print("/gps/ene/type User")
        print("/gps/hist/type energy")
        print("/gps/hist/point 0 0")
        # energy in MeV
        print("/gps/hist/point 100000 0")
        print("/gps/hist/point 3500000 1")
    else:
        print("/gps/ene/mono %s GeV" % (energy))

    # shoot photons with an angle if required
    if not straight:
        print("/gps/ang/type user")
        print("/gps/hist/type theta")
        print("/gps/hist/point 0. 0.")
        print("/gps/hist/point 0.01 1.")
# 2 photons
else:
    print("### source 1 ####")
    print("/gps/particle gamma")
    print("/gps/pos/type Plane")
    print("/gps/pos/shape Circle")
    print("/gps/pos/centre %.4g %.4g %.4g mm" % (randomX,randomY,randomZ))
    print("/gps/pos/radius "+str(distance)+" mm")
    print("/gps/direction 0 0 1")
    if energy == 'random':
        print("/gps/ene/type User")
        print("/gps/hist/type energy")
        print("/gps/hist/point 0 0")
        print("/gps/hist/point 100000 0")
        print("/gps/hist/point 3500000 1\n")
    else:
        print("/gps/ene/mono %s GeV" % (energy))

    print("### source 2 ####")
    print("/gps/source/add 1")
    print("/gps/particle gamma")
    print("/gps/pos/type Plane")
    print("/gps/pos/shape Circle")
    print("/gps/pos/centre %.4g %.4g %.4g mm" % (randomX,randomY,randomZ))
    print("/gps/pos/radius "+str(distance)+" mm")
    print("/gps/direction 0 0 1")
    if energy == 'random':
        print("/gps/ene/type User")
        print("/gps/hist/type energy")
        print("/gps/hist/point 0 0")
        print("/gps/hist/point 100000 0")
        print("/gps/hist/point 3500000 1")
    else:
        print("/gps/ene/mono %s GeV" % (energy))
    print("/gps/source/multiplevertex 1")

