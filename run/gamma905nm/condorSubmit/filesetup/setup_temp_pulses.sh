#!/bin/bash


# argument 1 = jobNumber, argument 2 = model, argument 3 = mass, argument 4 = coupling
jobNumber=${1}
model=${2}
gain=${3}
n_photons=${4}


export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
export DQ2_LOCAL_SITE_ID=DESY-HH_SCRATCHDISK
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
sleep 1 # sleeping is healthy
cd /afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_3p03/allpix-squared/
source /afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_3p03/allpix-squared/etc/scripts/setup_lxplus.sh
sleep 1

cd /afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_3p03/allpix-squared/run/gamma905nm/allpix/create_pulses/

#binary=`which allpix`
binary=/afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_3p03/allpix-squared/bin/allpix

#$binary -c transient_simulation_pointcharge.conf -o output_directory="output/" -o random_seed=${jobNumber} -o root_file="transient_modules_pointcharge_${model}_${gain}_${jobNumber}" -o ROOTObjectWriter.file_name="transient_data_pointcharge_${model}_${gain}_${jobNumber}" -o TransientPropagation.multiplication_model=${gain} 
$binary -c transient_simulation_laser.conf -o output_directory="output/" -o random_seed=${jobNumber} -o root_file="transient_modules_laser_${model}_${gain}_${n_photons}photons_${jobNumber}" -o ROOTObjectWriter.file_name="transient_data_laser_${model}_${gain}_${n_photons}photons_${jobNumber}" -o TransientPropagation.multiplication_model=${gain} -o DepositionLaser.number_of_photons=${n_photons}

# copy the output file to eos
cp -v /afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_3p03/allpix-squared/run/gamma905nm/allpix/create_pulses/output/transient_data_laser_${model}_${gain}_${n_photons}photons_${jobNumber}.root /eos/user/j/jsabater/unige/gamma905nm/allpix/
cp -v /afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_3p03/allpix-squared/run/gamma905nm/allpix/create_pulses/output/transient_modules_laser_${model}_${gain}_${n_photons}photons_${jobNumber}.root /eos/user/j/jsabater/unige/gamma905nm/allpix

# remove the local file
rm -v /afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_3p03/allpix-squared/run/gamma905nm/allpix/create_pulses/output/transient_data_laser_${model}_${gain}_${n_photons}photons_${jobNumber}.root
rm -v /afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_3p03/allpix-squared/run/gamma905nm/allpix/create_pulses/output/transient_modules_laser_${model}_${gain}_${n_photons}photons_${jobNumber}.root



