#include <iostream>
#include <fstream>
#include <TFile.h>
#include <TDirectoryFile.h>
#include <TGraph.h>

//int main() {
int pulses_to_csv() {

  int event_counter = 0;


  // Open the ROOT file
  int n_files = 10;
  //  int n_files = 1;
  for (int ifile = 0; ifile < n_files ; ifile++) {
    
    TString sfile_counter = "";
    sfile_counter += ifile + 1;
    //TString input_file = "/eos/user/j/jsabater/unige/gamma905nm/allpix/create_pulses/transient_modules_laser_gamma905nm_okuto_1photons_1.root";
    //TString input_file = "/eos/user/j/jsabater/unige/gamma905nm/allpix/create_pulses/beamWaist_25um_1x1matrix/transient_modules_laser_gamma905nm_okuto_1photons_1.root";
    //TString input_file = "/eos/user/j/jsabater/unige/gamma905nm/allpix/create_pulses/beamWaist_25um_1x1matrix/transient_modules_laser_gamma905nm_okuto_10photons_1.root";
    //TString input_file = "/eos/user/j/jsabater/unige/gamma905nm/allpix/create_pulses/beamWaist_25um_1x1matrix/transient_modules_laser_gamma905nm_okuto_100photons_1.root";
    //    TString input_file = "/eos/user/j/jsabater/unige/gamma905nm/allpix/create_pulses/beamWaist_25um_1x1matrix/transient_modules_laser_gamma905nm_okuto_1000photons_"+sfile_counter+".root";
    //       TString input_file = "/eos/user/j/jsabater/unige/gamma905nm/allpix/create_pulses/beamWaist_uniform29um_1x1matrix/transient_modules_laser_gamma905nm_okuto_4000photons_"+sfile_counter+".root";
    //TString input_file = "/eos/user/j/jsabater/unige/gamma905nm/allpix/create_pulses/beamWaist_uniform29um_1x1matrix/transient_modules_laser_gamma905nm_okuto_400photons_"+sfile_counter+".root";
    TString input_file = "/eos/user/j/jsabater/unige/gamma905nm/allpix/create_pulses/beamWaist_uniform29um_3x2matrix/transient_modules_laser_gamma905nm_okuto_4000photons_"+sfile_counter+".root";
    std::cout << "Opening " << input_file << std::endl;
    TFile* rootFile = TFile::Open(input_file, "READ");

  // Check if the file is opened successfully
  if (!rootFile || rootFile->IsZombie()) {
    std::cerr << "Error: Could not open ROOT file." << std::endl;
    return 1;
  }

  // Navigate to the TDirectoryFile 'PulseTransfer'
  TDirectoryFile* pulseTransferDir = dynamic_cast<TDirectoryFile*>(rootFile->Get("PulseTransfer"));

  if (!pulseTransferDir) {
    std::cerr << "Error: Could not find 'PulseTransfer' directory." << std::endl;
    rootFile->Close();
    return 1;
  }

  // Navigate to the TDirectoryFile 'detector1' within 'PulseTransfer'
  TDirectoryFile* detector1Dir = dynamic_cast<TDirectoryFile*>(pulseTransferDir->Get("detector1"));

  if (!detector1Dir) {
    std::cerr << "Error: Could not find 'detector1' directory." << std::endl;
    rootFile->Close();
    return 1;
  }



  int n_events = 10;
  // int n_events = 1000;
  //  int n_events = 300000;

  std::cout << "Running over " << n_events << " events!!" << std::endl;
  for (int iev = 0; iev < n_events; iev++) {
    TString sgraph_counter = "";
    sgraph_counter += iev+1;
    event_counter++;
    TString sevent_counter = "";
    sevent_counter += event_counter;

    // Get the TGraph 'current_ev1_px0_0' within 'detector1'
    // indices of the pixels from allpix
    std::vector <pair<int,int>> pixel_index_vec={{0,0},{0,1},{1,0},{1,1},{2,-1},{2,0}};

    double charge_max = 0;
    int chargeMax_index_x = 0;
    int chargeMax_index_y = 0;
    // take only the pixel with the maximum charge
    for (const std::pair<int, int>& pair : pixel_index_vec) {
	TGraph* charge_graph_tmp = dynamic_cast<TGraph*>(detector1Dir->Get("charge_ev"+sgraph_counter+"_px"+pair.first+"_"+pair.second));
	if (!charge_graph_tmp) continue;
	int nPoints = charge_graph_tmp->GetN();
	Double_t *x = charge_graph_tmp->GetX();
	Double_t *y = charge_graph_tmp->GetY();
	Double_t pixel_time = x[nPoints - 1];
	// take the absolute value since the measure charge is in electrons (and it has a negative value)
	Double_t pixel_total_charge = abs(y[nPoints - 1]);
	if (pixel_total_charge > charge_max) {
	  charge_max = pixel_total_charge;
	  chargeMax_index_x = pair.first;
	  chargeMax_index_y = pair.second;
	}
    }
    TGraph* graph = dynamic_cast<TGraph*>(detector1Dir->Get("current_ev"+sgraph_counter+"_px"+chargeMax_index_x+"_"+chargeMax_index_y));
    
  if (!graph) {
    //    std::cerr << "Error: Could not find 'current_ev"+sgraph_counter+"_px"+chargeMax_index_x+"_"+chargeMax_index_y+"' graph." << std::endl;
    //    std::cerr << "Continuing with next graph..." << std::endl;
    continue;
    rootFile->Close();
    return 1;
  }

  // Open a CSV file for writing
  std::ofstream outputFile("pulses/gamma905nm_pulse"+sevent_counter+".csv");
  //  std::ofstream outputFile("/eos/user/j/jsabater/unige/monolith/pulses_antonio/monolith2023/monolith2023_pulse"+sevent_counter+".csv");

  if (!outputFile.is_open()) {
    std::cerr << "Error: Could not open CSV file for writing." << std::endl;
    rootFile->Close();
    return 1;
  }


  // add leakage current at the beginning of the pulse
  //float I_leakage = 277e-15; // Ampere
  float I_leakage = 0.; // Ampere
  //  outputFile <<  0.0 << "," << I_leakage << "\n";
  // Write the (x, y) values of the TGraph to the CSV file
  for (int ip = 0; ip < graph->GetN(); ++ip) {
    double x, y;
    graph->GetPoint(ip, x, y);

    // remove the allpix value at 0 (since we added a value at 0 with leakage)
    //    if (y==0) {
    //      continue;
    //	}

    // shift all the plot by 0.1ns so we can add a leakage current of 100pA
    //float offset = 0.01e-9; // s
    float offset = 0.; // s
    outputFile << x*1.e-9 + offset << "," << y*1.e-6 << "\n";
  }

  // add leakage current at the end of the pulse
  // outputFile <<  1.10e-9 << "," << I_leakage << "\n";
  //  outputFile <<  2.10e-9 << "," << I_leakage << "\n";

  
  // Close the output CSV file and the ROOT file
  outputFile.close();

  }
  
  rootFile->Close();
  }
  std::cout << "Data has been successfully saved to 'output.csv'." << std::endl;

  return 0;
}
