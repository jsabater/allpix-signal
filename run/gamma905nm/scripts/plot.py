#import ROOT
import csv
import matplotlib.pyplot as plt
from array import array
import numpy as np
import os.path

n_events = 10
n_pixels = 5

# plot the pulse of a given event
do_plot = False
event_to_plot = 19


for iev in range(1,n_events+1):
    x_values_tot = []
    y_values_tot = []
    for ipixel in range(n_pixels+1):
        x_values = []
        y_values = []
        input_file = 'data/gamma905nm_pulse_ev'+str(iev)+'_graph'+str(ipixel)+'.csv'
        if not os.path.isfile(input_file):
            continue
        with open(input_file, 'r') as file:
            csv_reader = csv.reader(file)

            # Skip the header row if it exists
            next(csv_reader, None)
            # Read the data and store it in the lists
            #for row in csv_reader:
            for row_index, row in enumerate(csv_reader):
                if not row:
                    break
                if row[0].isspace():
                    continue
                x_value_tmp = float(row[0])*1e9 # in ns
                y_value_tmp = float(row[1])*1e3 # in mV
                x_values.append(x_value_tmp)
                y_values.append(y_value_tmp)
        x_values_tot.append(x_values)
        y_values_tot.append(y_values)
    
        plt.plot(x_values_tot, y_values_tot)  # Create a simple line plot with markers
        plt.title('')
        plt.xlabel('Time [ns]')
        plt.ylabel('Amplitude [mV]')
        
        plt.grid(True)  # Add a grid to the plot if desired
        
        # Set the x and y-axis tick labels to scientific notation
        plt.ticklabel_format(style='sci', axis='both', scilimits=(0,0))
        
        # Set the range of x-axis
        plt.xlim(0, 10.)
        #plt.xlim(0, 2.)
        plt.show()  # Display the plot



