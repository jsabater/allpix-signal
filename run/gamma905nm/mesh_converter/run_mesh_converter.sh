#VOLTAGE=130
#VOLTAGE=190
VOLTAGE=220
DIR_MPS="/eos/user/j/jsabater/unige/gamma905nm/${VOLTAGE}V/"
NAME_INPUT_FILE="w_field_gamma905nm"
#NAME_INPUT_FILE="e_field_gamma905nm"
NAME_OUTPUT_FILE="gamma905nm"
../../../bin/mesh_converter -c mesh_converter.conf -f ${DIR_MPS}/${NAME_INPUT_FILE} -o ${NAME_OUTPUT_FILE}
