#!/bin/bash  

root -l <<EOF
.L /afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_3p03/allpix-squared/lib/libAllpixObjects.so
.L analysisExample_matte.C
//.L analysisExample_matte_fit.C
//.L analysisExample_matte.C++
//.L analysisExample_matte_fit.C++


analysisExample("/afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_3p03/allpix-squared/run/gamma905nm/allpix/wavelength_studies/output/transient_data_laser_950nm.root", "detector1");
/*
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/space-charge_studies/1000photons/beamWaist_29um/group100/transient_data_laser_gamma905nm_okuto_1000photons_1.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/space-charge_studies/1000photons/beamWaist_29um/group100/transient_data_laser_gamma905nm_okuto_1000photons_2.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/space-charge_studies/1000photons/beamWaist_29um/group100/transient_data_laser_gamma905nm_okuto_1000photons_3.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/space-charge_studies/1000photons/beamWaist_29um/group100/transient_data_laser_gamma905nm_okuto_1000photons_4.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/space-charge_studies/1000photons/beamWaist_29um/group100/transient_data_laser_gamma905nm_okuto_1000photons_5.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/space-charge_studies/1000photons/beamWaist_29um/group100/transient_data_laser_gamma905nm_okuto_1000photons_6.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/space-charge_studies/1000photons/beamWaist_29um/group100/transient_data_laser_gamma905nm_okuto_1000photons_7.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/space-charge_studies/1000photons/beamWaist_29um/group100/transient_data_laser_gamma905nm_okuto_1000photons_8.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/space-charge_studies/1000photons/beamWaist_29um/group100/transient_data_laser_gamma905nm_okuto_1000photons_9.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/space-charge_studies/1000photons/beamWaist_29um/group100/transient_data_laser_gamma905nm_okuto_1000photons_10.root", "detector1");
*/


/*
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/space-charge_studies/100photons/Rover2/transient_data_laser_gamma905nm_okuto_100photons_1.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/space-charge_studies/100photons/Rover2/transient_data_laser_gamma905nm_okuto_100photons_2.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/space-charge_studies/100photons/Rover2/transient_data_laser_gamma905nm_okuto_100photons_3.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/space-charge_studies/100photons/Rover2/transient_data_laser_gamma905nm_okuto_100photons_4.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/space-charge_studies/100photons/Rover2/transient_data_laser_gamma905nm_okuto_100photons_5.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/space-charge_studies/100photons/Rover2/transient_data_laser_gamma905nm_okuto_100photons_6.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/space-charge_studies/100photons/Rover2/transient_data_laser_gamma905nm_okuto_100photons_7.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/space-charge_studies/100photons/Rover2/transient_data_laser_gamma905nm_okuto_100photons_8.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/space-charge_studies/100photons/Rover2/transient_data_laser_gamma905nm_okuto_100photons_9.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/space-charge_studies/100photons/Rover2/transient_data_laser_gamma905nm_okuto_100photons_10.root", "detector1");
*/

/*
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/create_pulses/transient_data_laser_gamma905nm_okuto_1photons_1.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/create_pulses/transient_data_laser_gamma905nm_okuto_1photons_2.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/create_pulses/transient_data_laser_gamma905nm_okuto_1photons_3.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/create_pulses/transient_data_laser_gamma905nm_okuto_1photons_4.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/create_pulses/transient_data_laser_gamma905nm_okuto_1photons_5.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/create_pulses/transient_data_laser_gamma905nm_okuto_1photons_6.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/create_pulses/transient_data_laser_gamma905nm_okuto_1photons_7.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/create_pulses/transient_data_laser_gamma905nm_okuto_1photons_8.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/create_pulses/transient_data_laser_gamma905nm_okuto_1photons_9.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/create_pulses/transient_data_laser_gamma905nm_okuto_1photons_10.root", "detector1");
*/


/*
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/timestep_0p1/transient_data_laser_gamma905nm_okuto_100photons_1.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/timestep_0p1/transient_data_laser_gamma905nm_okuto_100photons_2.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/timestep_0p1/transient_data_laser_gamma905nm_okuto_100photons_3.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/timestep_0p1/transient_data_laser_gamma905nm_okuto_100photons_4.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/timestep_0p1/transient_data_laser_gamma905nm_okuto_100photons_5.root", "detector1");

analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/timestep_0p1/transient_data_laser_gamma905nm_none_100photons_1.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/timestep_0p1/transient_data_laser_gamma905nm_none_100photons_2.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/timestep_0p1/transient_data_laser_gamma905nm_none_100photons_3.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/timestep_0p1/transient_data_laser_gamma905nm_none_100photons_4.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/timestep_0p1/transient_data_laser_gamma905nm_none_100photons_5.root", "detector1");
*/


// 10 photon
/*
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/transient_data_laser_gamma905nm_okuto_10photons_1.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/transient_data_laser_gamma905nm_okuto_10photons_2.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/transient_data_laser_gamma905nm_okuto_10photons_3.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/transient_data_laser_gamma905nm_okuto_10photons_4.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/transient_data_laser_gamma905nm_okuto_10photons_5.root", "detector1");
*/
/*
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/220V/transient_data_laser_gamma905nm_okuto_optimized_10photons_1.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/220V/transient_data_laser_gamma905nm_okuto_optimized_10photons_2.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/220V/transient_data_laser_gamma905nm_okuto_optimized_10photons_3.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/220V/transient_data_laser_gamma905nm_okuto_optimized_10photons_4.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/220V/transient_data_laser_gamma905nm_okuto_optimized_10photons_5.root", "detector1");

analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/220V/transient_data_laser_gamma905nm_overstraeten_optimized_10photons_1.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/220V/transient_data_laser_gamma905nm_overstraeten_optimized_10photons_2.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/220V/transient_data_laser_gamma905nm_overstraeten_optimized_10photons_3.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/220V/transient_data_laser_gamma905nm_overstraeten_optimized_10photons_4.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/220V/transient_data_laser_gamma905nm_overstraeten_optimized_10photons_5.root", "detector1");

analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/220V/transient_data_laser_gamma905nm_bologna_10photons_1.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/220V/transient_data_laser_gamma905nm_bologna_10photons_2.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/220V/transient_data_laser_gamma905nm_bologna_10photons_3.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/220V/transient_data_laser_gamma905nm_bologna_10photons_4.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/220V/transient_data_laser_gamma905nm_bologna_10photons_5.root", "detector1");
*/

// 100 photon
/*
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/220V/transient_data_laser_gamma905nm_okuto_optimized_100photons_1.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/220V/transient_data_laser_gamma905nm_okuto_optimized_100photons_2.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/220V/transient_data_laser_gamma905nm_okuto_optimized_100photons_3.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/220V/transient_data_laser_gamma905nm_okuto_optimized_100photons_4.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/220V/transient_data_laser_gamma905nm_okuto_optimized_100photons_5.root", "detector1");
*/

// 1000 photon
//analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/220V/transient_data_laser_gamma905nm_none_1000photons_1.root", "detector1");
//analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/220V/transient_data_laser_gamma905nm_okuto_1000photons_1.root", "detector1");


//analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/220V/transient_data_pointcharge_gamma905nm_none_1.root", "detector1");
//analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/220V/transient_data_pointcharge_gamma905nm_okuto_1.root", "detector1");
//analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/220V/transient_data_pointcharge_gamma905nm_okuto_optimized_1.root", "detector1");

// laser photon
// 1 photon
/*
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/transient_data_laser_gamma905nm_none_1photons_1.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/transient_data_laser_gamma905nm_okuto_1photons_1.root", "detector1");

// 10 photon
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/transient_data_laser_gamma905nm_none_10photons_1.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/transient_data_laser_gamma905nm_okuto_10photons_1.root", "detector1");

// 100 photon
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/transient_data_laser_gamma905nm_none_100photons_1.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/transient_data_laser_gamma905nm_okuto_100photons_1.root", "detector1");

// 1000 photon
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/transient_data_laser_gamma905nm_none_1000photons_1.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/gamma905nm/allpix/transient_data_laser_gamma905nm_okuto_1000photons_1.root", "detector1");
*/
// MIP
//analysisExample("/afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_new/allpix-squared/run/gamma905nm/allpix/output/transient_data_pointcharge_gamma905nm_okuto_1.root", "detector1");
//analysisExample("/afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_new/allpix-squared/run/gamma905nm/allpix/output/transient_data_pointcharge_gamma905nm_none_1.root", "detector1");

EOF

