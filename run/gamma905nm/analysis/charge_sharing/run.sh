#!/bin/bash  

root -l <<EOF
.L /afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_3p03/allpix-squared/lib/libAllpixObjects.so
.L charge_sharing.C
//.L analysisExample_matte_fit.C
//.L analysisExample_matte.C++
//.L analysisExample_matte_fit.C++
/*
charge_sharing("/afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_3p03/allpix-squared/run/gamma905nm/allpix/charge_sharing_studies/output/transient_data_laser_Rover32.root", "detector1");
charge_sharing("/afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_3p03/allpix-squared/run/gamma905nm/allpix/charge_sharing_studies/output/transient_data_laser_Rover16.root", "detector1");
charge_sharing("/afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_3p03/allpix-squared/run/gamma905nm/allpix/charge_sharing_studies/output/transient_data_laser_Rover8.root", "detector1");
charge_sharing("/afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_3p03/allpix-squared/run/gamma905nm/allpix/charge_sharing_studies/output/transient_data_laser_Rover4.root", "detector1");
charge_sharing("/afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_3p03/allpix-squared/run/gamma905nm/allpix/charge_sharing_studies/output/transient_data_laser_Rover2.root", "detector1");
*/

//charge_sharing("/afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_3p03/allpix-squared/run/gamma905nm/allpix/charge_sharing_studies/output/transient_data_laser_3edge.root", "detector1");



charge_sharing("/afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_3p03/allpix-squared/run/gamma905nm/allpix/charge_sharing_studies/test/output/transient_data_laser_3edge.root", "detector1");


EOF

