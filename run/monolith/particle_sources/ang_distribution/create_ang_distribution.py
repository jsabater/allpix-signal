import numpy as np
import matplotlib.pyplot as plt
import math

# Define the parameters for the Gaussian distribution
mean = 0.  # Mean of the distribution
sigma = 0.09*2*math.pi/360.  # radians (0.09 deg)
num_bins = 100  # Number of bins
#x_low = -5.*sigma
x_low = 0
x_high = 5.*sigma

# Create a histogram using numpy
n_points = 100000
data = np.random.normal(mean, sigma, n_points)
#hist_values, bin_edges = np.histogram(data, bins=num_bins, range=(x_low, x_high), density = True)
hist_values, bin_edges = np.histogram(data, bins=num_bins, range=(x_low, x_high))

integral = np.sum(hist_values)
hist_values_normalized = hist_values / integral

print(np.sum(hist_values_normalized))

# Open a file for writing
with open("angular_distribution.txt", "w") as file:
    file.write("/gps/ang/type user\n")
    file.write("/gps/hist/type theta\n")
    
    # Write histogram points
    for i, value in enumerate(hist_values_normalized):
        bin_center = (bin_edges[i] + bin_edges[i + 1]) / 2.0
        file.write(f"/gps/hist/point {bin_center:.4f} {value}\n")



# Plot the normalized histogram
plt.bar(bin_edges[:-1], hist_values_normalized, width=bin_edges[1] - bin_edges[0], align='center', alpha=0.7, color='b')
plt.title("Normalized Histogram of Gaussian Distribution")
plt.xlabel("Value")
plt.ylabel("Normalized Probability Density")
plt.grid(True)


# Plot the histogram
#plt.hist(data, bins=num_bins, range=(x_low, x_high), density=True, alpha=0.7, color='b')
#plt.title("Gaussian Distribution")
#plt.xlabel("Value")
#plt.ylabel("Probability Density")
#plt.grid(True)

# Plot the Gaussian curve
#x = np.linspace(x_low, x_high, 100)
#y = (1 / (sigma * np.sqrt(2 * np.pi))) * np.exp(-((x - mean) ** 2) / (2 * sigma ** 2))
#plt.plot(x, y, 'r-', linewidth=2)

# Show the plot
plt.show()
