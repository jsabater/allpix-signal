#!/bin/bash  





root -l ../../allpix/monolith2023_50umepi/output/transient_data_25charges/transient_data_monolith2023_50umepi.root <<EOF
.L /afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_new/allpix-squared/lib/libAllpixObjects.so
.L constructComparisonTree.C++
auto file = new TFile("output/output.root", "RECREATE")
auto tree = constructComparisonTree(_file0, "detector1")
tree->Write()
EOF

