#!/bin/bash  

root -l <<EOF
.L /afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_new/allpix-squared/lib/libAllpixObjects.so
.L analysisExample_matte.C
//.L analysisExample_matte_fit.C
//.L analysisExample_matte.C++
//.L analysisExample_matte_fit.C++
// 120V
// monolith2023
//analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2023/allpix/25ps_spacing/transient_data_monolith2023.root", "detector1");
//analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2023/allpix/1ps_spacing/transient_data_monolith2023.root", "detector1");
//analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2023/allpix/1ps_spacing/threshold700e_noise100e/transient_data_monolith2023.root", "detector1");

// monolith2023_50umepi
//analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2023_50umepi/allpix/1ps_spacing/transient_data_monolith2023_50umepi.root", "detector1");
//analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2023_50umepi/allpix/1ps_spacing/threshold700e_noise100e/transient_data_monolith2023_50umepi.root", "detector1");


// 200V
// monolith2023
//analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2023_50umepi/allpix/1ps_spacing/200V/transient_data_monolith2023_50umepi.root", "detector1");
// monolith2023_50umepi
//analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2023/allpix/1ps_spacing/200V/transient_data_monolith2023.root", "detector1");


// for tests
analysisExample("/afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_new/allpix-squared/run/monolith/allpix/monolith2023/output/transient_data_monolith2023_1.root", "detector1");

EOF

