#!/bin/bash  

root -l <<EOF
.L /afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_new/allpix-squared/lib/libAllpixObjects.so
.L analysisExample_matte.C
//.L analysisExample_matte_fit.C
//.L analysisExample_matte.C++
//.L analysisExample_matte_fit.C++
// 120V
// monolith2023
//analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2023/allpix/25ps_spacing/transient_data_monolith2023.root", "detector1");
//analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2023/allpix/1ps_spacing/transient_data_monolith2023.root", "detector1");
//analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2023/allpix/1ps_spacing/threshold700e_noise100e/transient_data_monolith2023.root", "detector1");

// monolith2023_50umepi
//analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2023_50umepi/allpix/1ps_spacing/transient_data_monolith2023_50umepi.root", "detector1");
//analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2023_50umepi/allpix/1ps_spacing/threshold700e_noise100e/transient_data_monolith2023_50umepi.root", "detector1");


// 200V
// monolith2023
//analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2023_50umepi/allpix/1ps_spacing/200V/transient_data_monolith2023_50umepi.root", "detector1");
// monolith2023_50umepi
//analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2023/allpix/1ps_spacing/200V/transient_data_monolith2023.root", "detector1");


// monolith2023
//analysisExample("/afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_new/allpix-squared/run/monolith/allpix/monolith2023/output/transient_data_center.root", "detector1");
//analysisExample("/afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_new/allpix-squared/run/monolith/allpix/monolith2023/output/transient_data_edge.root", "detector1");	
/*
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2023/allpix/25ps_spacing/transient_data_monolith2023_1.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2023/allpix/25ps_spacing/transient_data_monolith2023_2.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2023/allpix/25ps_spacing/transient_data_monolith2023_3.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2023/allpix/25ps_spacing/transient_data_monolith2023_4.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2023/allpix/25ps_spacing/transient_data_monolith2023_5.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2023/allpix/25ps_spacing/transient_data_monolith2023_6.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2023/allpix/25ps_spacing/transient_data_monolith2023_7.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2023/allpix/25ps_spacing/transient_data_monolith2023_8.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2023/allpix/25ps_spacing/transient_data_monolith2023_9.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2023/allpix/25ps_spacing/transient_data_monolith2023_10.root", "detector1");
*/

// monolith2023_50umepi
/*
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2023_50umepi/allpix/25ps_spacing/200V/transient_data_monolith2023_50umepi_1.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2023_50umepi/allpix/25ps_spacing/200V/transient_data_monolith2023_50umepi_2.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2023_50umepi/allpix/25ps_spacing/200V/transient_data_monolith2023_50umepi_3.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2023_50umepi/allpix/25ps_spacing/200V/transient_data_monolith2023_50umepi_4.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2023_50umepi/allpix/25ps_spacing/200V/transient_data_monolith2023_50umepi_5.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2023_50umepi/allpix/25ps_spacing/200V/transient_data_monolith2023_50umepi_6.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2023_50umepi/allpix/25ps_spacing/200V/transient_data_monolith2023_50umepi_7.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2023_50umepi/allpix/25ps_spacing/200V/transient_data_monolith2023_50umepi_8.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2023_50umepi/allpix/25ps_spacing/200V/transient_data_monolith2023_50umepi_9.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2023_50umepi/allpix/25ps_spacing/200V/transient_data_monolith2023_50umepi_10.root", "detector1");
*/


// monolith2022
/*
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2022/allpix/25ps_spacing/transient_data_monolith2022_1.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2022/allpix/25ps_spacing/transient_data_monolith2022_2.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2022/allpix/25ps_spacing/transient_data_monolith2022_3.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2022/allpix/25ps_spacing/transient_data_monolith2022_4.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2022/allpix/25ps_spacing/transient_data_monolith2022_5.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2022/allpix/25ps_spacing/transient_data_monolith2022_6.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2022/allpix/25ps_spacing/transient_data_monolith2022_7.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2022/allpix/25ps_spacing/transient_data_monolith2022_8.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2022/allpix/25ps_spacing/transient_data_monolith2022_9.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2022/allpix/25ps_spacing/transient_data_monolith2022_10.root", "detector1");
*/


// monolith2022_50umepi
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2022_50umepi/allpix/25ps_spacing/transient_data_monolith2022_50umepi_1.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2022_50umepi/allpix/25ps_spacing/transient_data_monolith2022_50umepi_2.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2022_50umepi/allpix/25ps_spacing/transient_data_monolith2022_50umepi_3.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2022_50umepi/allpix/25ps_spacing/transient_data_monolith2022_50umepi_4.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2022_50umepi/allpix/25ps_spacing/transient_data_monolith2022_50umepi_5.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2022_50umepi/allpix/25ps_spacing/transient_data_monolith2022_50umepi_6.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2022_50umepi/allpix/25ps_spacing/transient_data_monolith2022_50umepi_7.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2022_50umepi/allpix/25ps_spacing/transient_data_monolith2022_50umepi_8.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2022_50umepi/allpix/25ps_spacing/transient_data_monolith2022_50umepi_9.root", "detector1");
analysisExample("/eos/user/j/jsabater/unige/monolith/monolith2022_50umepi/allpix/25ps_spacing/transient_data_monolith2022_50umepi_10.root", "detector1");



EOF

