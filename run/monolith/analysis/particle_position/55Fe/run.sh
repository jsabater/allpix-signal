#!/bin/bash  

root -l <<EOF
.L /afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_new/allpix-squared/lib/libAllpixObjects.so
.L analysisExample_matte.C
//.L analysisExample_matte_fit.C
//.L analysisExample_matte.C++
//.L analysisExample_matte_fit.C++


// monolith2023
analysisExample("/afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_new/allpix-squared/run/monolith/allpix/monolith2023/output/transient_data_55Fe.root", "detector1");

EOF

