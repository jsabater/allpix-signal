#include <iostream>
#include <fstream>
#include <TFile.h>
#include <TDirectoryFile.h>
#include <TGraph.h>

//int main() {
int pulses_to_csv_filter_55Fe() {

  int event_counter = 0;


  // Open the ROOT file
  //    int n_files = 10;
  int n_files = 1;
  for (int ifile = 0; ifile < n_files ; ifile++) {
    
    TString sfile_counter = "";
    sfile_counter += ifile + 1;
    TString input_file = "/afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_new/allpix-squared/run/monolith/allpix/monolith2023/output/transient_modules_55Fe.root";
    
    //    TString input_file = "/eos/user/j/jsabater/unige/monolith/monolith2023/allpix/1ps_spacing//transient_modules_monolith2023_"+sfile_counter+".root";
    //TString input_file = "/eos/user/j/jsabater/unige/monolith/monolith2023/allpix/25ps_spacing//transient_modules_monolith2023_"+sfile_counter+".root";
    //TString input_file = "/eos/user/j/jsabater/unige/monolith/monolith2023/allpix/transient_modules_pointcharge_63ehum_25psspacing.root";
    //TString input_file = "/eos/user/j/jsabater/unige/monolith/monolith2023/allpix/transient_modules_pointcharge_63ehum_1psspacing_edge.root";

    //TString input_file = "/afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_new/allpix-squared/run/monolith/allpix/monolith2023/output/transient_modules_center.root";
    //TString input_file = "/afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_new/allpix-squared/run/monolith/allpix/monolith2023/output/transient_modules_edge.root";
    std::cout << "Opening " << input_file << std::endl;
    TFile* rootFile = TFile::Open(input_file, "READ");

  // Check if the file is opened successfully
  if (!rootFile || rootFile->IsZombie()) {
    std::cerr << "Error: Could not open ROOT file." << std::endl;
    return 1;
  }

  // Navigate to the TDirectoryFile 'PulseTransfer'
  TDirectoryFile* pulseTransferDir = dynamic_cast<TDirectoryFile*>(rootFile->Get("PulseTransfer"));

  if (!pulseTransferDir) {
    std::cerr << "Error: Could not find 'PulseTransfer' directory." << std::endl;
    rootFile->Close();
    return 1;
  }

  // Navigate to the TDirectoryFile 'detector1' within 'PulseTransfer'
  TDirectoryFile* detector1Dir = dynamic_cast<TDirectoryFile*>(pulseTransferDir->Get("detector1"));

  if (!detector1Dir) {
    std::cerr << "Error: Could not find 'detector1' directory." << std::endl;
    rootFile->Close();
    return 1;
  }


  
  //  TString filename_mcparticles = "/afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_new/allpix-squared/run/monolith/analysis/particle_position/output/monolith2023/mc_particles_"+sfile_counter+".root";
  TString filename_mcparticles = "/afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_new/allpix-squared/run/monolith/analysis/particle_position/55Fe/output/mc_particles_e.root";
  TFile *file_mcparticles = TFile::Open(filename_mcparticles,"READ");
  if (!file_mcparticles || file_mcparticles->IsZombie()) {
    std::cerr << "Error: Cannot open file " << filename_mcparticles << std::endl;
    return 1;
  }
  // Access the TTree in the file
  TTree *tree_mcparticles = (TTree*)file_mcparticles->Get("myTree");
  if (!tree_mcparticles) {
    std::cerr << "Error: Cannot find TTree in file" << std::endl;
    file_mcparticles->Close();
    return 1;
  }

  
  // Variables to store the values from the branch
  Double_t QfC;
  
  // Set branch addresses
  tree_mcparticles->SetBranchAddress("QfC", &QfC);
  
  int n_events = tree_mcparticles->GetEntries();
  //int n_events = 1000;
  
  for (int iev = 0; iev < n_events; iev++) {
    tree_mcparticles->GetEntry(iev);

	
    TString sgraph_counter = "";
    sgraph_counter += iev+1;
    event_counter++;
    TString sevent_counter = "";
    sevent_counter += event_counter;

    
    // remove events where the particle didn't hit the detector
    double threshold = 0.2;
    if (abs(QfC) < threshold) {
      std::cout << "event " << sevent_counter << " skipped" << std::endl;
      continue;
    }
    
    // Get the TGraph 'current_ev1_px0_0' within 'detector1'
    // indices of the pixels from allpix
    std::vector <pair<int,int>> pixel_index_vec={{0,0},{0,1},{1,0},{1,1},{2,-1},{2,0}};

    double charge_max = 0;
    int chargeMax_index_x = 0;
    int chargeMax_index_y = 0;
    // take only the pixel with the maximum charge
    for (const std::pair<int, int>& pair : pixel_index_vec) {
	TGraph* charge_graph_tmp = dynamic_cast<TGraph*>(detector1Dir->Get("charge_ev"+sgraph_counter+"_px"+pair.first+"_"+pair.second));
	if (!charge_graph_tmp) continue;
	int nPoints = charge_graph_tmp->GetN();
	Double_t *x = charge_graph_tmp->GetX();
	Double_t *y = charge_graph_tmp->GetY();
	Double_t pixel_time = x[nPoints - 1];
	// take the absolute value since the measure charge is in electrons (and it has a negative value)
	Double_t pixel_total_charge = abs(y[nPoints - 1]);
	if (pixel_total_charge > charge_max) {
	  charge_max = pixel_total_charge;
	  chargeMax_index_x = pair.first;
	  chargeMax_index_y = pair.second;
	}
    }
    TGraph* graph = dynamic_cast<TGraph*>(detector1Dir->Get("current_ev"+sgraph_counter+"_px"+chargeMax_index_x+"_"+chargeMax_index_y));
    
  if (!graph) {
    //    std::cerr << "Error: Could not find 'current_ev"+sgraph_counter+"_px"+chargeMax_index_x+"_"+chargeMax_index_y+"' graph." << std::endl;
    //    std::cerr << "Continuing with next graph..." << std::endl;
    continue;
    rootFile->Close();
    return 1;
  }

  // Open a CSV file for writing
  std::ofstream outputFile("pulses/monolith2023_pulse"+sevent_counter+".csv");
  //  std::ofstream outputFile("/eos/user/j/jsabater/unige/monolith/pulses_antonio/monolith2023/monolith2023_pulse"+sevent_counter+".csv");

  if (!outputFile.is_open()) {
    std::cerr << "Error: Could not open CSV file for writing." << std::endl;
    rootFile->Close();
    return 1;
  }

  
  // add leakage current at the beginning of the pulse
  //float I_leakage = 277e-15; // Ampere
  float I_leakage = 0.; // Ampere
  //  outputFile <<  0.0 << "," << I_leakage << "\n";
  // Write the (x, y) values of the TGraph to the CSV file
  for (int ip = 0; ip < graph->GetN(); ++ip) {
    double x, y;
    graph->GetPoint(ip, x, y);

    // remove the allpix value at 0 (since we added a value at 0 with leakage)
    //    if (y==0) {
    //      continue;
    //	}

    // shift all the plot by 0.1ns so we can add a leakage current of 100pA
    //float offset = 0.01e-9; // s
    float offset = 0.; // s
    outputFile << x*1.e-9 + offset << "," << y*1.e-6 << "\n";
  }

  // add leakage current at the end of the pulse
  outputFile <<  2.10e-9 << "," << I_leakage << "\n";
  
  
  // Close the output CSV file and the ROOT file
  outputFile.close();

  }
  
  rootFile->Close();
  }
  std::cout << "Data has been successfully saved to 'output.csv'." << std::endl;

  return 0;
}
