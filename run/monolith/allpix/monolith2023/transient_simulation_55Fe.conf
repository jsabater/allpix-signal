[Allpix]
#log_level = "WARNING"
log_level = "INFO"
log_format = "DEFAULT"
detectors_file = "monolith2023_detector.conf"
model_paths = "../../models/"
root_file = "transient_modules_55Fe"
#number_of_events = 500000
#number_of_events = 300000
#number_of_events = 10000
number_of_events = 10
multithreading = true
workers = 4
random_seed = 123


[GeometryBuilderGeant4]

# # [Ignore] # replace by [VisualizationGeant4] to run the visualization
#[VisualizationGeant4] # enable to run the visualization
#mode = "gui"

[DepositionGeant4]
physics_list = FTFP_BERT_LIV
particle_type = "Fe55"
source_energy = 0GeV
source_position = 0um 0um -143um
#source_type = "point"
source_type = "square"
square_side = 150um
max_step_length = 0.1um
number_of_particles = 1


[ElectricFieldReader]
#log_level = "DEBUG"
log_level = "WARNING"
model = "mesh"
file_name = "/afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_new/allpix-squared/run/monolith/mesh_converter/monolith2023/monolith2023_ElectricField.init"
#depletion_depth = 25um
field_mapping = pixel_full # how your simulated structure is mapped into the allpix-squared geometry
output_plots = true
output_plots_project = z
#output_plots_projection_percentage = 0.3
#output_plots_projection_percentage = 0.5
output_plots_projection_percentage = 0.8
output_plots_single_pixel = false
output_plots_steps = 200


[WeightingPotentialReader]
log_level = "TRACE"
model = "mesh"
field_mapping = pixel_full
file_name = "/afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_new/allpix-squared/run/monolith/mesh_converter/monolith2023/monolith2023_ElectrostaticPotential.init"
#file_name = "/afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_new/allpix-squared/run/monolith/mesh_converter/monolith2023/test/monolith2023_ElectrostaticPotential.init"
output_plots = true
#ignore_field_dimensions = true
#output_plots_position = -2,0
output_plots_single_pixel = false
output_plots_steps = 200


[DopingProfileReader]
model = "mesh"
file_name = "/afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_new/allpix-squared/run/monolith/mesh_converter/monolith2023/monolith2023_DopingConcentration.init"
field_mapping = pixel_full
output_plots = true
#output_plots_project = z
#output_plots_projection_percentage = 0.3
output_plots_single_pixel = false
output_plots_steps = 200

[TransientPropagation]
log_level = "WARNING"
temperature = 293K
#charge_per_step = 25 # to tune according to how the pulses look like
charge_per_step = 10 # to tune according to how the pulses look like
#charge_per_step = 1 # to tune according to how the pulses look like
#charge_per_step = 500 # to tune according to how the pulses look like
#charge_per_step = 5000 # to tune according to how the pulses look like
#timestep_max = 1ps #*
#timestep = 1ps #*
timestep_max = 25ps #*
timestep = 25ps #*
integration_time = 2ns
# distance = 1 # number of pixels considered for current induction, default to 1
ignore_magnetic_field = true
output_plots = false
output_linegraphs = false # tracking the charge moving inside the sensors
#output_plots_lines_at_implants = true # only plot charge carrier that reached the implant
output_linegraphs_collected = false # only plot charge carrier that reached the implant
#output_animations = true # animation of charge moving inside the sensors
mobility_model = "masetti_canali" #masetti_canali" #"arora" #"masetti_canali" #
recombination_model = "srh_auger"
#multiplication_model = "okuto" #"overstraeten" # "okuto" #
#multiplication_threshold = 15kV/cm

[PulseTransfer]
log_level = "INFO"
output_pulsegraphs = true
output_plots = false
#output_plots_scale = 3000

[DefaultDigitizer]
log_level = "INFO"
electronics_noise = 145e
#electronics_noise = 100e
output_plots = false
output_plots_scale = 1
output_plots_timescale = 1ns
threshold = 1015e
#threshold = 700e
threshold_smearing = 50e
#threshold_smearing = 35e
qdc_smearing = 0
tdc_smearing = 0
#saturation = true

[DetectorHistogrammer]
name = "detector1"
max_cluster_charge = 20000

[ROOTObjectWriter]
#include = MCTrack, MCParticle, PixelHit, DepositedCharge, PropagatedCharge, PixelCharge
include = PixelHit, PixelCharge, MCParticle
file_name = "transient_data_55Fe"

#[TextWriter]
#include = "PixelHit","PixelCharge","MCParticle"
