# Allpix Squared ASCII data - https://cern.ch/allpix-squared

=== 223 ===
--- detector1 ---

--- Printing MCParticle information (0x56175b516d20) --------------------
Particle type (PDG ID):     211
Local start point:        0.0433013  mm |     0.025  mm |     0.035 mm  
Global start point:               0  mm |    0.0125  mm |    -0.035 mm  
Local end point:          0.0433012  mm |     0.025  mm |    -0.035 mm  
Global end point:        -1.98738e-08  mm |    0.0125  mm |     0.035 mm  
Local time:                       0  ns 
Global time:             0.000317053  ns 
Linked parent:           <nullptr>
Linked track:            0x561761681870
-------------------------------------------------------------------------

--- detector1 ---
--- Pixel charge information
Pixel: (0, 0)
Charge: 1 e
Local Position: (0, 0, 0) mm
Global Position: (-0.0433013, 0.0375, -4.59243e-18) mm
Local time:0 ns
Global time:0.000317053 ns

--- Pixel charge information
Pixel: (0, 1)
Charge: 1 e
Local Position: (0, 0.05, 0) mm
Global Position: (-0.0433013, -0.0125, 1.53081e-18) mm
Local time:0 ns
Global time:0.000317053 ns

--- Pixel charge information
Pixel: (1, 0)
Charge: -3874 e
Local Position: (0.0433013, 0.025, 0) mm
Global Position: (0, 0.0125, -1.53081e-18) mm
Local time:0 ns
Global time:0.000317053 ns

--- Pixel charge information
Pixel: (1, 1)
Charge: 0 e
Local Position: (0.0433013, 0.075, 0) mm
Global Position: (0, -0.0375, 4.59243e-18) mm
Local time:0 ns
Global time:0.000317053 ns

--- Pixel charge information
Pixel: (2, -1)
Charge: 1 e
Local Position: (0.0866025, 0, 0) mm
Global Position: (0.0433013, 0.0375, -4.59243e-18) mm
Local time:0 ns
Global time:0.000317053 ns

--- Pixel charge information
Pixel: (2, 0)
Charge: 1 e
Local Position: (0.0866025, 0.05, 0) mm
Global Position: (0.0433013, -0.0125, 1.53081e-18) mm
Local time:0 ns
Global time:0.000317053 ns

--- detector1 ---
PixelHit 1, 0, 3947.09, 0.099, 0.0993171, 0, 0.0125, -1.53081e-18
=== 224 ===
--- detector1 ---

--- Printing MCParticle information (0x56175b516d20) --------------------
Particle type (PDG ID):     211
Local start point:        0.0433013  mm |     0.025  mm |     0.035 mm  
Global start point:               0  mm |    0.0125  mm |    -0.035 mm  
Local end point:          0.0433014  mm | 0.0250001  mm |    -0.035 mm  
Global end point:        1.36998e-07  mm | 0.0124999  mm |     0.035 mm  
Local time:                       0  ns 
Global time:             0.000317053  ns 
Linked parent:           <nullptr>
Linked track:            0x561761681870
-------------------------------------------------------------------------

--- detector1 ---
--- Pixel charge information
Pixel: (0, 0)
Charge: 1 e
Local Position: (0, 0, 0) mm
Global Position: (-0.0433013, 0.0375, -4.59243e-18) mm
Local time:0 ns
Global time:0.000317053 ns

--- Pixel charge information
Pixel: (0, 1)
Charge: 1 e
Local Position: (0, 0.05, 0) mm
Global Position: (-0.0433013, -0.0125, 1.53081e-18) mm
Local time:0 ns
Global time:0.000317053 ns

--- Pixel charge information
Pixel: (1, 0)
Charge: -3922 e
Local Position: (0.0433013, 0.025, 0) mm
Global Position: (0, 0.0125, -1.53081e-18) mm
Local time:0 ns
Global time:0.000317053 ns

--- Pixel charge information
Pixel: (1, 1)
Charge: 1 e
Local Position: (0.0433013, 0.075, 0) mm
Global Position: (0, -0.0375, 4.59243e-18) mm
Local time:0 ns
Global time:0.000317053 ns

--- Pixel charge information
Pixel: (2, -1)
Charge: 0 e
Local Position: (0.0866025, 0, 0) mm
Global Position: (0.0433013, 0.0375, -4.59243e-18) mm
Local time:0 ns
Global time:0.000317053 ns

--- Pixel charge information
Pixel: (2, 0)
Charge: 0 e
Local Position: (0.0866025, 0.05, 0) mm
Global Position: (0.0433013, -0.0125, 1.53081e-18) mm
Local time:0 ns
Global time:0.000317053 ns

--- detector1 ---
PixelHit 1, 0, 3870.66, 0.098, 0.0983171, 0, 0.0125, -1.53081e-18
# 16 objects from 6 messages
