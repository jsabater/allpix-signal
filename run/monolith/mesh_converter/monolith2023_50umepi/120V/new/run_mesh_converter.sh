DIR_MPS="/eos/user/j/jsabater/unige/monolith/monolith2023_50umepi/"
NAME_INPUT_FILE="w_field_monolith2023_50umepi"
#NAME_INPUT_FILE="e_field_monolith2023_50umepi"
NAME_OUTPUT_FILE="monolith2023_50umepi"
../../../../../bin/mesh_converter -c mesh_converter.conf -f ${DIR_MPS}/${NAME_INPUT_FILE} -o ${NAME_OUTPUT_FILE}
