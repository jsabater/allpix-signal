DIR_MPS="/eos/user/j/jsabater/unige/monolith/monolith2023_50umepi/200V/"
#NAME_INPUT_FILE="w_field_monolith2023_50umepi_200V"
NAME_INPUT_FILE="e_field_monolith2023_50umepi_200V"
NAME_OUTPUT_FILE="monolith2023_50umepi_200V"
../../../../../bin/mesh_converter -c mesh_converter.conf -f ${DIR_MPS}/${NAME_INPUT_FILE} -o ${NAME_OUTPUT_FILE}
