#!/bin/bash


# argument 1 = jobNumber, argument 2 = model, argument 3 = mass, argument 4 = coupling
jobNumber=${1}
model=${2}


export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
export DQ2_LOCAL_SITE_ID=DESY-HH_SCRATCHDISK
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
sleep 1 # sleeping is healthy
cd /afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_new/allpix-squared/
source /afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_new/allpix-squared/etc/scripts/setup_lxplus.sh
sleep 1

cd /afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_new/allpix-squared/run/faser/allpix/

#binary=`which allpix`
binary=/afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_new/allpix-squared/bin/allpix


$binary -c transient_simulation_200V.conf -o output_directory="output/" -o random_seed=${jobNumber} -o root_file="transient_modules_200V_${model}_${jobNumber}" -o ROOTObjectWriter.file_name="transient_data_200V_${model}_${jobNumber}"



