import os
from optparse import OptionParser
import numpy as np
parser = OptionParser()
parser.add_option('--model', help='model: ALP-W, ALP-photon', type=str, default="ALP-W")
(options, args) = parser.parse_args()

modelname = options.model

if modelname == "ALP-W":
    masses = np.logspace(-1.5,0.3,15)
    coups = np.logspace(-7,-2,21)
elif modelname == "ALP-photon":
    masses = np.logspace(-2,-0.3,20+1)
    coups = np.logspace(-6,-2,17)
elif modelname == "UpPhilic":
    masses = np.logspace(-1,-0.3,23)
    coups = np.logspace(-10,-2,17)
elif modelname == "U1B":
    masses = np.logspace(-1,0,21)
    coups = np.logspace(-8,-2,19)
else:
    print('Model not found')

n_jobs = 10
for mass in masses:
    integer_part, decimal_part = str(round(mass,3)).split(".")
    smass = integer_part + "p" + decimal_part
    for coup in coups:
        # transform 0.001 to 1.0e-3
        scoup_tmp = '{:5.1e}'.format(coup)
        integer, decimal = str(scoup_tmp).split(".")
        scoup = integer + "p" + decimal
        #for ijob in range(n_jobs):
        cmd="python run_allpix_condor_signal.py --jobNumber 1 --model "+modelname+" --mass "+smass+" --coup "+scoup
        #cmd="python run_allpix_condor_signal.py --jobNumber 1 --model "+modelname+" --mass "+mass+" --coup "+coup
        #cmd="python run_allpix_condor_signal.py --jobNumber "+str(ijob+1)+" --model "+modelname+" --mass "+mass+" --coup "+coup
        print(cmd)
        os.system(cmd)
