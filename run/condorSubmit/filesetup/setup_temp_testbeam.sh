#!/bin/bash


# argument 1 = jobNumber, argument 2 = photon energy, argument 3 = geometry, argument 4 = number of photons, argument 5 = separation
jobNumber=${1}
energy=${2}
geometry=${3}
nPhotons=${4}
separation=${5}

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
export DQ2_LOCAL_SITE_ID=DESY-HH_SCRATCHDISK
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
sleep 1 # sleeping is healthy
cd /afs/cern.ch/work/j/jsabater/unige/FASER/allpix-rafa/allpix-squared/
#source /cvmfs/clicdp.cern.ch/software/allpix-rafa/allpix-squared/2.2.1/x86_64-centos7-gcc11-opt/setup.sh
#source /cvmfs/clicdp.cern.ch/software/allpix-rafa/allpix-squared/2.3.0/x86_64-centos7-gcc11-opt/setup.sh
#source /cvmfs/clicdp.cern.ch/software/allpix-rafa/allpix-squared/latest/x86_64-centos7-gcc11-opt/setup.sh
source /afs/cern.ch/work/j/jsabater/unige/FASER/allpix-rafa/allpix-squared/etc/scripts/setup_lxplus.sh
sleep 1

cd /afs/cern.ch/work/j/jsabater/unige/FASER/allpix-rafa/allpix-squared/run/
#binary=`which allpix`
binary=/afs/cern.ch/work/j/jsabater/unige/FASER/allpix-rafa/allpix-squared/bin/allpix

geo_path="../geometry/testbeam/testbeamGeo.conf"
modules_outName="modules_${jobNumber}_${energy}GeV_${geometry}"
particle_source_path="../particle_sources/testbeam/1ele_${energy}GeV_0mm.mac"
outName="faser_${jobNumber}_${energy}GeV_${geometry}_${separation}mm"
$binary -c conf/benchmark_conf.conf -o output_directory="../output/${geometry}" -o detectors_file=${geo_path} -o random_seed=${jobNumber} -o root_file=${modules_outName} -o DepositionGeant4.file_name=${particle_source_path} -o CalibrationFlatTreeWriter.file_name=${outName}

