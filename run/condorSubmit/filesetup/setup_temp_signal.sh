#!/bin/bash


# argument 1 = jobNumber, argument 2 = model, argument 3 = mass, argument 4 = coupling
jobNumber=${1}
model=${2}
mass=${3}
coup=${4}


# some of the output from foresee may not have enough events to be saved, so first check that the hepmcfile exists and if not, exit the code
hepmc_file=/eos/project/f/faser-preshower/simulations/foresee/${model}/${model}_m${mass}_eps${coup}.hepmc
if [ ! -f "$hepmc_file" ]; then
    echo "$hep_mc_file does not exist, exiting..."
    return
fi



export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
export DQ2_LOCAL_SITE_ID=DESY-HH_SCRATCHDISK
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
sleep 1 # sleeping is healthy
cd /afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_new/allpix-squared/
#source /cvmfs/clicdp.cern.ch/software/allpix-squared_new/allpix-squared/2.2.1/x86_64-centos7-gcc11-opt/setup.sh
#source /cvmfs/clicdp.cern.ch/software/allpix-squared_new/allpix-squared/2.3.0/x86_64-centos7-gcc11-opt/setup.sh
#source /cvmfs/clicdp.cern.ch/software/allpix-squared_new/allpix-squared/latest/x86_64-centos7-gcc11-opt/setup.sh
source /afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_new/allpix-squared/etc/scripts/setup_lxplus.sh
sleep 1

cd /afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_new/allpix-squared/run/

#binary=`which allpix`
binary=/afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_new/allpix-squared/bin/allpix

$binary -c conf/hepmc/hepmc.conf -o output_directory="../../output/${model}" -o detectors_file="../../geometry/detailed/geometry_2x6mm4x2mm.conf" -o random_seed=${jobNumber} -o root_file="modules_${jobNumber}_${model}_m${mass}_eps${coup}" -o DepositionGenerator.file_name="/eos/project/f/faser-preshower/simulations/foresee/${model}/${model}_m${mass}_eps${coup}.hepmc" -o CalibrationFlatTreeWriter.file_name="faser_${jobNumber}_${model}_m${mass}_eps${coup}"

# copy the output file to eos
cp -v /afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_new/allpix-squared/run/output/${model}/CalibrationFlatTreeWriter/faser_${jobNumber}_${model}_m${mass}_eps${coup}.root /eos/project/f/faser-preshower/simulations/allpix/signal/${model}/

# remove the local file
rm -v /afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared_new/allpix-squared/run/output/${model}/CalibrationFlatTreeWriter/faser_${jobNumber}_${model}_m${mass}_eps${coup}.root



