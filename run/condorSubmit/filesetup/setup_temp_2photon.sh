#!/bin/bash


# argument 1 = jobNumber, argument 2 = photon energy, argument 3 = geometry, argument 4 = number of photons, argument 5 = separation, argument 6 = straight
jobNumber=${1}
energy=${2}
geometry=${3}
nPhotons=${4}
separation=${5}
straight=${6}  # shoot photons straight, =0 is straight, =1 is with angle

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
export DQ2_LOCAL_SITE_ID=DESY-HH_SCRATCHDISK
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh
sleep 1 # sleeping is healthy
cd /afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared/allpix-squared/
#source /cvmfs/clicdp.cern.ch/software/allpix-squared/allpix-squared/2.2.1/x86_64-centos7-gcc11-opt/setup.sh
#source /cvmfs/clicdp.cern.ch/software/allpix-squared/allpix-squared/2.3.0/x86_64-centos7-gcc11-opt/setup.sh
#source /cvmfs/clicdp.cern.ch/software/allpix-squared/allpix-squared/latest/x86_64-centos7-gcc11-opt/setup.sh
source /afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared/allpix-squared/etc/scripts/setup_lxplus.sh
sleep 1

# create the particle source config file
cd /afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared/allpix-squared/run/particle_sources/randomPosition

suffix=""
if (( straight == '1' )); 
# straight photons
then
    python createConfig.py -d ${separation} -n ${nPhotons} -s ${jobNumber} -e ${energy} --straight > ${nPhotons}gamma_${energy}GeV_${separation}mm_randomPosition_${jobNumber}.mac
    suffix="straight"
else
# photons with angle
    python createConfig.py -d ${separation} -n ${nPhotons} -s ${jobNumber} -e ${energy} > ${nPhotons}gamma_${energy}GeV_${separation}mm_randomPosition_${jobNumber}.mac
fi

sleep 1

cd /afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared/allpix-squared/run/
#binary=`which allpix`
binary=/afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared/allpix-squared/bin/allpix

$binary -c conf/benchmark_conf.conf -o output_directory="../output/${geometry}" -o detectors_file="../geometry/detailed/geometry_${geometry}.conf" -o random_seed=${jobNumber} -o root_file="modules_${jobNumber}_${nPhotons}gamma_${energy}GeV_${separation}mm"${suffix} -o DepositionGeant4.file_name="../particle_sources/randomPosition/${nPhotons}gamma_${energy}GeV_${separation}mm_randomPosition_${jobNumber}.mac" -o ROOTObjectWriter.file_name="faser_${jobNumber}_${energy}GeV_${geometry}_${nPhotons}photon_${separation}mm"${suffix} -o CalibrationFlatTreeWriter.file_name="faser_${jobNumber}_${energy}GeV_${geometry}_${nPhotons}photon_${separation}mm"${suffix}

# remove the temporary particle source file
rm -v /afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared/allpix-squared/run/particle_sources/randomPosition/${nPhotons}gamma_${energy}GeV_${separation}mm_randomPosition_${jobNumber}.mac

# copy the output file to eos
cp -v /afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared/allpix-squared/run/output/${geometry}/CalibrationFlatTreeWriter/faser_${jobNumber}_${energy}GeV_${geometry}_${nPhotons}photon_${separation}mm${suffix}.root /eos/project/f/faser-preshower/simulations/allpix/${nPhotons}photon/${geometry}/

# remove the local file
rm -v /afs/cern.ch/work/j/jsabater/unige/FASER/allpix-squared/allpix-squared/run/output/${geometry}/CalibrationFlatTreeWriter/faser_${jobNumber}_${energy}GeV_${geometry}_${nPhotons}photon_${separation}mm${suffix}.root 



