#!/usr/bin/python
import os
import subprocess
from optparse import OptionParser
from datetime import date
import sys
import logging
import htcondor
import classad
import shutil
import time
import gc

# possible  geometries = 3p5W, 3p5W_3X0, 4p6W, 3p5W_3X0_2Si, 3p5W_2X0_2Si, 2x6mm4x2mm
########################
####### 1 photon #######
########################

#######################
## Straight photons ###
#######################
# 1k events of 1 photon with random energies from 100 GeV to 3.5 TeV
# python run_allpix_condor.py --jobNumber 10 --energy random --geometry 3p5W -n 1 --separation 0 --straight 1


#######################
#### With Angles ######
#######################
# 1k events of 1 photon of 1 TeV
# python run_allpix_condor.py --jobNumber 10 --energy 1000 --geometry 3p5W -n 1 --separation 0
# random energies
# python run_allpix_condor.py --jobNumber 10 --energy random --geometry 3p5W -n 1 --separation 0



##########################
######## 2 photons #######
##########################

# 1k events, 2 photons with energies of 1TeV
# python run_allpix_condor.py --jobNumber 10 --energy 1000 --geometry 3p5W -n 2 --separation 0p2
# random GeV
# python run_allpix_condor.py --jobNumber 10 --energy random --geometry 3p5W -n 2 --separation 0p2


# to submit testbeam 2022 (electrons 20,50,100,150 GeV)
# python run_allpix_condor.py --jobNumber 10 --energy 20 --geometry testbeam -n 1 --separation 0
collector = htcondor.Collector()
mySchedd = htcondor.Schedd()
credd = htcondor.Credd()
credd.add_user_cred(htcondor.CredTypes.Kerberos, None)

parser = OptionParser()
parser.add_option('-c', '--jobNumber', help='--jobNumber 1', type=int, default=1)
#parser.add_option('-o', '--outputName', help='--outputName g4', type=str, default='g4')
parser.add_option('-i', '--input', help='allpix output file', type=str, default='/eos/user/j/jsabater/unige/FASER/allpix/noNoiseCenteredASIC/faser.root')
parser.add_option('-e', '--energy', help='photon energy in GeV', type=str, default='1000')
parser.add_option('-g', '--geometry', help='geometry, possilibities 3p5W, 3p5W_3X0, 4p6W, 3p5W_3X0_2Si', type=str, default='3p5W')
parser.add_option('-s', '--separation', help='separation between photons, e.g. 0p2, 2 in mm', type=str, default='0')
parser.add_option('-n', '--nPhotons', help='number of photons, 1 or 2', type=str, default='1')
#parser.add_option("-b", '--doSingleJob',action="store_true", dest="verbose", default=False)
parser.add_option('-b','--doSingleJob', default=False, action='store_true')
parser.add_option('--straight', help='shoot straight photons', type=int, default=0) # shoot photons straight, =0 is with angle, =1 is straight

(options, args) = parser.parse_args()
jobNumber = options.jobNumber
#outputName = options.outputName
inputFile = options.input
energy = options.energy
geometry = options.geometry
separation = options.separation
nPhotons = options.nPhotons
doSingleJob = options.doSingleJob
straight = options.straight

#def sendjobs(detector):
def sendjobs():
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', datefmt='%H:%M:%S')
    log = logging.getLogger("mySubmissionLogger")
    log.setLevel(logging.INFO)
    log.info('Loading options ...')
    pwd=os.getcwd()
    myjob = htcondor.Submit()

    folder_log = os.getcwd() + "/output_condor/"
    dateToday = date.today()
    data = dateToday.strftime("%b-%d-%Y")
    #myjob["+TRUST_UID_DOMAIN"] = "TRUE"
    #namefilesetup='filesetup/setup_temp.sh'
    namefilesetup=""
    if (geometry == "testbeam"):
        namefilesetup='filesetup/setup_temp_testbeam.sh'
    else:
        namefilesetup='filesetup/setup_temp_2photon.sh'
    myjob["Executable"] = namefilesetup
    myjob["request_cpus"] = '4'
    #myjob["request_cpus"] = '1'
    myjob["transfer_executable"] = True
    myjob["JobBatchName"] = namefilesetup
    myjob["when_to_transfer_output"] = "ON_EXIT"
    myjob["universe"] = "vanilla"
    myjob["notification"] = "Always"
    myjob["Output"] = folder_log+"allpix_"+data+"_$(ClusterId).$(ProcId).out"
    myjob["Error"] = folder_log+"allpix_"+data+"_$(ClusterId).$(ProcId).error"
    myjob["Log"] = folder_log+"allpix_"+data+"_$(ClusterId).$(ProcId).log"
    #myjob["+JobFlavour"] = '"espresso"' # espresso = 20min, microcentury = 1 hour
    #myjob["+JobFlavour"] = '"microcentury"' # espresso = 20min, microcentury = 1 hour
    myjob["+JobFlavour"] = '"workday"' # espresso = 20min, microcentury = 1 hour, longlunch = 2 hours, workday = 8 hours
    #myjob["+JobFlavour"] = '"longlunch"' # espresso = 20min, microcentury = 1 hour
    myjob['MY.SendCredential'] = True # apparently needed in lxplus

    with mySchedd.transaction() as myTransaction:
        if doSingleJob:
            myjob["Arguments"] = str(jobNumber)
            myjob["Arguments"] += ' '+energy
            myjob["Arguments"] += ' '+geometry
            myjob["Arguments"] += ' '+nPhotons
            myjob["Arguments"] += ' '+separation
            myjob["Arguments"] += ' '+str(straight)
            time.sleep(0.3)
            log.info('Queue...')
            print ('Submitting job '+str(jobNumber))
            myjob.queue(myTransaction)
            log.info('Submission done')
        else:
            for job in range(jobNumber):
                print ('Submitting job')
                #myjob["Arguments"] = job
                myjob["Arguments"] = str(job+1)
                myjob["Arguments"] += ' '+energy
                myjob["Arguments"] += ' '+geometry
                myjob["Arguments"] += ' '+nPhotons
                myjob["Arguments"] += ' '+separation
                myjob["Arguments"] += ' '+str(straight)
                time.sleep(0.3)
                log.info('Queue...')
                myjob.queue(myTransaction)
                log.info('Submission done')
            
       
if __name__ == "__main__":        
    sendjobs()
        
