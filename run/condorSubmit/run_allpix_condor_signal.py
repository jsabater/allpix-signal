#!/usr/bin/python
import os
import subprocess
from optparse import OptionParser
from datetime import date
import sys
import logging
import htcondor
import classad
import shutil
import time
import gc

# possible models: ALP-W
# python run_allpix_condor_signal.py --jobNumber 1 --model ALP-W --mass 0p2 --coup 1p0e-04


collector = htcondor.Collector()
mySchedd = htcondor.Schedd()
credd = htcondor.Credd()
credd.add_user_cred(htcondor.CredTypes.Kerberos, None)

parser = OptionParser()
parser.add_option('-c', '--jobNumber', help='--jobNumber 1', type=int, default=1)
parser.add_option('--model', help='model ALP-W, ...', type=str, default="ALP-W")
parser.add_option('--mass', help='mass of ALP', type=str, default="0p2")
parser.add_option('--coup', help='coupling', type=str, default="1p0e-04")

parser.add_option('-b','--doSingleJob', default=False, action='store_true')

(options, args) = parser.parse_args()
jobNumber = options.jobNumber
model = options.model
mass = options.mass
coup = options.coup
doSingleJob = options.doSingleJob


#def sendjobs(detector):
def sendjobs():
    logging.basicConfig(format='%(asctime)s %(levelname)s %(message)s', datefmt='%H:%M:%S')
    log = logging.getLogger("mySubmissionLogger")
    log.setLevel(logging.INFO)
    log.info('Loading options ...')
    pwd=os.getcwd()
    myjob = htcondor.Submit()

    folder_log = os.getcwd() + "/output_condor/"
    dateToday = date.today()
    data = dateToday.strftime("%b-%d-%Y")
    #myjob["+TRUST_UID_DOMAIN"] = "TRUE"
    namefilesetup="filesetup/setup_temp_signal.sh"

    myjob["Executable"] = namefilesetup
    myjob["request_cpus"] = '4'
    #myjob["request_cpus"] = '1'
    myjob["transfer_executable"] = True
    myjob["JobBatchName"] = namefilesetup
    myjob["when_to_transfer_output"] = "ON_EXIT"
    myjob["universe"] = "vanilla"
    myjob["notification"] = "Always"
    myjob["Output"] = folder_log+"allpix_"+data+"_$(ClusterId).$(ProcId).out"
    myjob["Error"] = folder_log+"allpix_"+data+"_$(ClusterId).$(ProcId).error"
    myjob["Log"] = folder_log+"allpix_"+data+"_$(ClusterId).$(ProcId).log"
    #myjob["+JobFlavour"] = '"espresso"' # espresso = 20min, microcentury = 1 hour
    #myjob["+JobFlavour"] = '"microcentury"' # espresso = 20min, microcentury = 1 hour
    #myjob["+JobFlavour"] = '"workday"' # espresso = 20min, microcentury = 1 hour, longlunch = 2 hours, workday = 8 hours, tomorrow = 1 day
    myjob["+JobFlavour"] = '"tomorrow"' # espresso = 20min, microcentury = 1 hour, longlunch = 2 hours, workday = 8 hours, tomorrow = 1 day
    #myjob["+JobFlavour"] = '"testmatch"' # espresso = 20min, microcentury = 1 hour, testmatch = 3 days
    myjob['MY.SendCredential'] = True # apparently needed in lxplus

    with mySchedd.transaction() as myTransaction:
        if doSingleJob:
            myjob["Arguments"] = str(jobNumber)
            myjob["Arguments"] += ' '+model
            myjob["Arguments"] += ' '+mass
            myjob["Arguments"] += ' '+coup
            time.sleep(0.3)
            log.info('Queue...')
            print ('Submitting job '+str(jobNumber))
            myjob.queue(myTransaction)
            log.info('Submission done')
        else:
            for job in range(jobNumber):
                print ('Submitting job')
                #myjob["Arguments"] = job
                myjob["Arguments"] = str(job+1)
                myjob["Arguments"] += ' '+model
                myjob["Arguments"] += ' '+mass
                myjob["Arguments"] += ' '+coup
                time.sleep(0.3)
                log.info('Queue...')
                myjob.queue(myTransaction)
                log.info('Submission done')
            
       
if __name__ == "__main__":        
    sendjobs()
        
