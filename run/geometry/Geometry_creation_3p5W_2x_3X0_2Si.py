chip_type = "faser"

asic_size_y = 15.280
asic_size_x = 22.150

number_of_asics_y = 12
number_of_asics_x = 6
number_of_layers = 2

tungsten_absorber_x = 200
tungsten_absorber_y = 200
tungsten_absorber_z = 3.5

tungsten_absorber_3X0 = 3.5*3.
tungsten_absorber_2X0 = 3.5*2.

aluminum_cooling_x = 194
aluminum_cooling_y = 194
aluminum_cooling_z = 5

aluminum_module_x = 75.1*2
#aluminum_module_y = 31.2*3 + 25.2*3
aluminum_module_y = 31.2*6
aluminum_module_z = 6.5

sisi_gap = 15 # 15mm between silicon layers

#silicon_z = 0.150 # real one
silicon_z = 0.068 # current one
 
offset_x = asic_size_x * (number_of_asics_x - 1) / 2
offset_y = asic_size_y * (number_of_asics_y - 1) / 2

# full layer thickness = 3.5mm W +  5mm alu + 6.5mm alu + 0.068mm silicon + 30mm air  (in real life is 0.15mm silicon)
#distance_between_layers = 45.15
distance_between_layers = 30.
# additional gap due to the 2 silicon layers on the first layers
additional_gap = sisi_gap + aluminum_cooling_z + aluminum_module_z + silicon_z


# the position corresponds to the center of the volume
for layer in range(number_of_layers):
    if (layer == 0):
        print('[tungsten_%s]' % layer)
        print('type = "box"')
        print('size = %gmm %gmm %gmm' % (tungsten_absorber_x, tungsten_absorber_y, tungsten_absorber_3X0))
        print('position = 0mm 0mm %gmm' % (layer * distance_between_layers - 15.000 + tungsten_absorber_3X0/2.))
        print('orientation = 0 0 0')
        print('material = tungsten')
        print('role = "passive"\n')
        
        # first Aluminum + silicon layer
        print('[aluminum_cooling_%s]' % layer)
        print('type = "box"')
        print('size = %gmm %gmm %gmm' % (aluminum_cooling_x, aluminum_cooling_y, aluminum_cooling_z))
        print('position = 0mm 0mm %gmm' % (layer * distance_between_layers - 15.000 + tungsten_absorber_3X0 + aluminum_cooling_z/2.))
        print('orientation = 0 0 0')
        print('material = aluminum')
        print('role = "passive"\n')
        
        print('[aluminum_module_%s]' % layer)
        print('type = "box"')
        print('size = %gmm %gmm %gmm' % (aluminum_module_x, aluminum_module_y, aluminum_module_z))
        print('position = 0mm 0mm %gmm' % (layer * distance_between_layers - 15.000 + tungsten_absorber_3X0 + aluminum_cooling_z + aluminum_module_z/2.))
        print('orientation = 0 0 0')
        print('material = aluminum')
        print('role = "passive"\n')
        
        for asic_x in range(number_of_asics_x):
            for asic_y in range(number_of_asics_y):
                print('[Detector_%d_%d_%d]' % (layer + 1, asic_x + 1, asic_y + 1))
                print('type = "%s"' % chip_type)
                pos_x = asic_x * asic_size_x - offset_x
                pos_y = asic_y * asic_size_y - offset_y - (0.46 if asic_x % 2 else 0)
                print('position = %gmm %gmm %gmm' % (pos_x, pos_y, layer * distance_between_layers - 15.000 + tungsten_absorber_3X0 + aluminum_cooling_z + aluminum_module_z + silicon_z/2.  ))
                print('orientation = 0deg 0deg %ddeg\n' % (180 if asic_x % 2 else 0))

        # 2nd Aluminum + silicon layer
        print('[aluminum_cooling_%s]' % str(layer+1))
        print('type = "box"')
        print('size = %gmm %gmm %gmm' % (aluminum_cooling_x, aluminum_cooling_y, aluminum_cooling_z))
        print('position = 0mm 0mm %gmm' % ( - 15.000 + tungsten_absorber_3X0 + aluminum_cooling_z + aluminum_module_z + silicon_z + sisi_gap + aluminum_cooling_z/2. ))
        print('orientation = 0 0 0')
        print('material = aluminum')
        print('role = "passive"\n')

        print('[aluminum_module_%s]' % str(layer+1))
        print('type = "box"')
        print('size = %gmm %gmm %gmm' % (aluminum_module_x, aluminum_module_y, aluminum_module_z))
        print('position = 0mm 0mm %gmm' % ( - 15.000 + tungsten_absorber_3X0 + aluminum_cooling_z +  aluminum_module_z + silicon_z + sisi_gap + aluminum_cooling_z + aluminum_module_z/2.))
        print('orientation = 0 0 0')
        print('material = aluminum')
        print('role = "passive"\n')

        for asic_x in range(number_of_asics_x):
            for asic_y in range(number_of_asics_y):
                print('[Detector_%d_%d_%d]' % (layer + 1 + 1, asic_x + 1, asic_y + 1))
                print('type = "%s"' % chip_type)
                pos_x = asic_x * asic_size_x - offset_x
                pos_y = asic_y * asic_size_y - offset_y - (0.46 if asic_x % 2 else 0)
                print('position = %gmm %gmm %gmm' % (pos_x, pos_y,  - 15.000 + tungsten_absorber_3X0 + aluminum_cooling_z + aluminum_module_z + silicon_z + sisi_gap + aluminum_cooling_z + aluminum_module_z + silicon_z/2. ))
                print('orientation = 0deg 0deg %ddeg\n' % (180 if asic_x % 2 else 0))
        
    else:
        print('[tungsten_%s]' % layer)
        print('type = "box"')
        print('size = %gmm %gmm %gmm' % (tungsten_absorber_x, tungsten_absorber_y, tungsten_absorber_3X0))
        print('position = 0mm 0mm %gmm' % (tungsten_absorber_2X0 + layer * distance_between_layers - 15.000 + additional_gap + tungsten_absorber_3X0/2.))
        print('orientation = 0 0 0')
        print('material = tungsten')
        print('role = "passive"\n')
        
        print('[aluminum_cooling_%s]' % str(layer+1))
        print('type = "box"')
        print('size = %gmm %gmm %gmm' % (aluminum_cooling_x, aluminum_cooling_y, aluminum_cooling_z))
        print('position = 0mm 0mm %gmm' % (tungsten_absorber_2X0 + layer * distance_between_layers - 15.000 + additional_gap + tungsten_absorber_3X0 +  aluminum_cooling_z/2.))
        print('orientation = 0 0 0')
        print('material = aluminum')
        print('role = "passive"\n')
        
        print('[aluminum_module_%s]' % str(layer+1))
        print('type = "box"')
        print('size = %gmm %gmm %gmm' % (aluminum_module_x, aluminum_module_y, aluminum_module_z))
        print('position = 0mm 0mm %gmm' % (tungsten_absorber_2X0 + layer * distance_between_layers - 15.000 + additional_gap + tungsten_absorber_3X0 + aluminum_cooling_z + aluminum_module_z/2.))
        print('orientation = 0 0 0')
        print('material = aluminum')
        print('role = "passive"\n')
        
        for asic_x in range(number_of_asics_x):
            for asic_y in range(number_of_asics_y):
                print('[Detector_%d_%d_%d]' % (layer + 1 + 1, asic_x + 1, asic_y + 1))
                print('type = "%s"' % chip_type)
                pos_x = asic_x * asic_size_x - offset_x 
                pos_y = asic_y * asic_size_y - offset_y - (0.46 if asic_x % 2 else 0)
                #print('position = %gmm %gmm %gmm' % (pos_x, pos_y,tungsten_absorber_2X0 + layer * distance_between_layers - 15.000 + tungsten_absorber_z + aluminum_cooling_z + aluminum_module_z + silicon_z/2.  ))
                print('position = %gmm %gmm %gmm' % (pos_x, pos_y,tungsten_absorber_2X0 + layer * distance_between_layers - 15.000 + additional_gap + tungsten_absorber_3X0 + aluminum_cooling_z + aluminum_module_z + silicon_z/2.))
                print('orientation = 0deg 0deg %ddeg\n' % (180 if asic_x % 2 else 0))


        # 2nd Aluminum + silicon layer
        print('[aluminum_cooling_%s]' % str(layer+2))
        print('type = "box"')
        print('size = %gmm %gmm %gmm' % (aluminum_cooling_x, aluminum_cooling_y, aluminum_cooling_z))
        print('position = 0mm 0mm %gmm' % ( tungsten_absorber_2X0 + layer * distance_between_layers - 15.000 + additional_gap + tungsten_absorber_3X0 + aluminum_cooling_z + aluminum_module_z + silicon_z + sisi_gap + aluminum_cooling_z/2. ))
        print('orientation = 0 0 0')
        print('material = aluminum')
        print('role = "passive"\n')
        
        print('[aluminum_module_%s]' % str(layer+2))
        print('type = "box"')
        print('size = %gmm %gmm %gmm' % (aluminum_module_x, aluminum_module_y, aluminum_module_z))
        print('position = 0mm 0mm %gmm' % ( tungsten_absorber_2X0 + layer * distance_between_layers - 15.000 + additional_gap + tungsten_absorber_3X0 + aluminum_cooling_z +  aluminum_module_z + silicon_z + sisi_gap + aluminum_cooling_z + aluminum_module_z/2.))
        print('orientation = 0 0 0')
        print('material = aluminum')
        print('role = "passive"\n')

        for asic_x in range(number_of_asics_x):
            for asic_y in range(number_of_asics_y):
                print('[Detector_%d_%d_%d]' % (layer + 1 + 2, asic_x + 1, asic_y + 1))
                print('type = "%s"' % chip_type)
                pos_x = asic_x * asic_size_x - offset_x
                pos_y = asic_y * asic_size_y - offset_y - (0.46 if asic_x % 2 else 0)
                print('position = %gmm %gmm %gmm' % (pos_x, pos_y, tungsten_absorber_2X0 + layer * distance_between_layers - 15.000 + additional_gap + tungsten_absorber_3X0 + aluminum_cooling_z + aluminum_module_z + silicon_z + sisi_gap + aluminum_cooling_z + aluminum_module_z + silicon_z/2. ))
                print('orientation = 0deg 0deg %ddeg\n' % (180 if asic_x % 2 else 0))
