# Telescope plane 0
alum_x = 114
alum_y = 206.5
alum_z = 5
alum_hole_x = 62
alum_hole_y = 14
z_increase =  alum_z/2.
print('[alum_%s]' % 0)
print('type = "box"')
print('size = %gmm %gmm %gmm' % (alum_x, alum_y, alum_z))
print('inner_size = %gmm %gmm %gmm' % (alum_hole_x,alum_hole_y,alum_z))
print('position = 0mm 0mm %gmm' % (z_increase))
print('orientation = 0 0 0')
print('material = aluminum')
print('role = "passive"\n')

IBL_x = 41.315
IBL_y = 18.585
IBL_z = 0.35
z_increase = z_increase + alum_z/2. + IBL_z/2.
print('[IBL_%s]' % 0)
print('type = "box"')
print('size = %gmm %gmm %gmm' % (IBL_x, IBL_y, IBL_z))
print('position = 0mm 0mm %gmm' % (z_increase))
print('orientation = 0 0 0')
print('material = silicon')
print('role = "passive"\n')

alum_thin_x = 74.
alum_thin_y = 74.
alum_thin_z = 0.13
z_increase = z_increase + IBL_z/2. + alum_thin_z/2.
print('[alum_thin_%s]' % 0)
print('type = "box"')
print('size = %gmm %gmm %gmm' % (alum_thin_x, alum_thin_y, alum_thin_z))
print('position = 0mm 0mm %gmm' % (z_increase))
print('orientation = 0 0 0')
print('material = aluminum')
print('role = "passive"\n')

# Telescope plane 1
d_Plane01 = 175

z_increase = z_increase + alum_thin_z/2. + d_Plane01 + alum_z/2.
print('[alum_%s]' % 1)
print('type = "box"')
print('size = %gmm %gmm %gmm' % (alum_x, alum_y, alum_z))
print('inner_size = %gmm %gmm %gmm' % (alum_hole_x,alum_hole_y,alum_z))
print('position = 0mm 0mm %gmm' % (z_increase))
print('orientation = 0deg 0deg 90deg')
print('material = aluminum')
print('role = "passive"\n')

z_increase = z_increase + alum_z/2. + IBL_z/2.
print('[IBL_%s]' % 1)
print('type = "box"')
print('size = %gmm %gmm %gmm' % (IBL_x, IBL_y, IBL_z))
print('position = 0mm 0mm %gmm' % (z_increase))
print('orientation = 0deg 0deg 90deg')
print('material = silicon')
print('role = "passive"\n')

z_increase = z_increase + IBL_z/2. + alum_thin_z/2.
print('[alum_thin_%s]' % 1)
print('type = "box"')
print('size = %gmm %gmm %gmm' % (alum_thin_x, alum_thin_y, alum_thin_z))
print('position = 0mm 0mm %gmm' % (z_increase))
print('orientation = 0deg 0deg 90deg')
print('material = aluminum')
print('role = "passive"\n')

# Telescope plane 2
d_Plane12 = 183

z_increase = z_increase + alum_thin_z/2. + d_Plane12 + alum_z/2.
print('[alum_%s]' % 2)
print('type = "box"')
print('size = %gmm %gmm %gmm' % (alum_x, alum_y, alum_z))
print('inner_size = %gmm %gmm %gmm' % (alum_hole_x,alum_hole_y,alum_z))
print('position = 0mm 0mm %gmm' % (z_increase))
print('orientation = 0 0 0')
print('material = aluminum')
print('role = "passive"\n')

z_increase = z_increase + alum_z/2. + IBL_z/2.
print('[IBL_%s]' % 2)
print('type = "box"')
print('size = %gmm %gmm %gmm' % (IBL_x, IBL_y, IBL_z))
print('position = 0mm 0mm %gmm' % (z_increase))
print('orientation = 0 0 0')
print('material = silicon')
print('role = "passive"\n')

z_increase = z_increase + IBL_z/2. + alum_thin_z/2.
print('[alum_thin_%s]' % 2)
print('type = "box"')
print('size = %gmm %gmm %gmm' % (alum_thin_x, alum_thin_y, alum_thin_z))
print('position = 0mm 0mm %gmm' % (z_increase))
print('orientation = 0 0 0')
print('material = aluminum')
print('role = "passive"\n')


# Telescope plane 3
d_Plane23 = 325

z_increase = z_increase + alum_thin_z/2. + d_Plane23 + alum_z/2.
print('[alum_%s]' % 3)
print('type = "box"')
print('size = %gmm %gmm %gmm' % (alum_x, alum_y, alum_z))
print('inner_size = %gmm %gmm %gmm' % (alum_hole_x,alum_hole_y,alum_z))
print('position = 0mm 0mm %gmm' % (z_increase))
print('orientation = 0 0 0')
print('material = aluminum')
print('role = "passive"\n')

z_increase = z_increase + alum_z/2. + IBL_z/2.
print('[IBL_%s]' % 3)
print('type = "box"')
print('size = %gmm %gmm %gmm' % (IBL_x, IBL_y, IBL_z))
print('position = 0mm 0mm %gmm' % (z_increase))
print('orientation = 0 0 0')
print('material = silicon')
print('role = "passive"\n')

z_increase = z_increase + IBL_z/2. + alum_thin_z/2.
print('[alum_thin_%s]' % 3)
print('type = "box"')
print('size = %gmm %gmm %gmm' % (alum_thin_x, alum_thin_y, alum_thin_z))
print('position = 0mm 0mm %gmm' % (z_increase))
print('orientation = 0 0 0')
print('material = aluminum')
print('role = "passive"\n')

# Telescope plane 4
d_Plane34 = 105

z_increase = z_increase + alum_thin_z/2. + d_Plane34 + alum_z/2.
print('[alum_%s]' % 4)
print('type = "box"')
print('size = %gmm %gmm %gmm' % (alum_x, alum_y, alum_z))
print('inner_size = %gmm %gmm %gmm' % (alum_hole_x,alum_hole_y,alum_z))
print('position = 0mm 0mm %gmm' % (z_increase))
print('orientation = 0deg 0deg 90deg')
print('material = aluminum')
print('role = "passive"\n')

z_increase = z_increase + alum_z/2. + IBL_z/2.
print('[IBL_%s]' % 4)
print('type = "box"')
print('size = %gmm %gmm %gmm' % (IBL_x, IBL_y, IBL_z))
print('position = 0mm 0mm %gmm' % (z_increase))
print('orientation = 0deg 0deg 90deg')
print('material = silicon')
print('role = "passive"\n')

z_increase = z_increase + IBL_z/2. + alum_thin_z/2.
print('[alum_thin_%s]' % 4)
print('type = "box"')
print('size = %gmm %gmm %gmm' % (alum_thin_x, alum_thin_y, alum_thin_z))
print('position = 0mm 0mm %gmm' % (z_increase))
print('orientation = 0deg 0deg 90deg')
print('material = aluminum')
print('role = "passive"\n')


# Telescope plane 5
d_Plane45 = 95

z_increase = z_increase + alum_thin_z/2. + d_Plane45 + alum_z/2.
print('[alum_%s]' % 5)
print('type = "box"')
print('size = %gmm %gmm %gmm' % (alum_x, alum_y, alum_z))
print('inner_size = %gmm %gmm %gmm' % (alum_hole_x,alum_hole_y,alum_z))
print('position = 0mm 0mm %gmm' % (z_increase))
print('orientation = 0 0 0')
print('material = aluminum')
print('role = "passive"\n')

z_increase = z_increase + alum_z/2. + IBL_z/2.
print('[IBL_%s]' % 5)
print('type = "box"')
print('size = %gmm %gmm %gmm' % (IBL_x, IBL_y, IBL_z))
print('position = 0mm 0mm %gmm' % (z_increase))
print('orientation = 0 0 0')
print('material = silicon')
print('role = "passive"\n')

z_increase = z_increase + IBL_z/2. + alum_thin_z/2.
print('[alum_thin_%s]' % 5)
print('type = "box"')
print('size = %gmm %gmm %gmm' % (alum_thin_x, alum_thin_y, alum_thin_z))
print('position = 0mm 0mm %gmm' % (z_increase))
print('orientation = 0 0 0')
print('material = aluminum')
print('role = "passive"\n')

# DUT plane 0
d_Plane5DUT = 60.694;
d_supportPre = 4 # tungsten support before the tungsten
d_supportPost = 3  # tungsten support after the tungste
d_supportPCB = 3; # distance from tungsten support to PCB
d_airGap = 11.94 + d_supportPre
d_W_PCB = d_supportPost + d_supportPCB
tungsten_x = 50.
tungsten_y = 100.
tungsten_z = 7.7
z_increase = z_increase + alum_thin_z/2. + d_Plane5DUT + tungsten_z/2.
print('[tungsten_%s]' % 0)
print('type = "box"')
print('size = %gmm %gmm %gmm' % (tungsten_x, tungsten_y, tungsten_z))
print('position = 0mm 10mm %gmm' % (z_increase))
print('orientation = 0 0 0')
print('material = tungsten')
print('role = "passive"\n')

copper_x = 100
copper_y = 80
copper_z = 0.106
copper_hole_x = 4
copper_hole_y = 5
z_increase = z_increase + tungsten_z/2. + copper_z/2. + d_W_PCB
print('[cooper_%s]' % 0)
print('type = "box"')
print('size = %gmm %gmm %gmm' % (copper_x, copper_y, copper_z))
print('inner_size = %gmm %gmm %gmm' % (copper_hole_x,copper_hole_y,copper_z))
print('position = 0mm 0mm %gmm' % (z_increase))
print('orientation = 0 0 0')
print('material = copper')
print('role = "passive"\n')

PCB_x = 100
PCB_y = 80
PCB_z = 1.5
PCB_hole_x = 4
PCB_hole_y = 5
z_increase = z_increase + copper_z/2. + PCB_z/2.
print('[PCB_%s]' % 0)
print('type = "box"')
print('size = %gmm %gmm %gmm' % (PCB_x, PCB_y, PCB_z))
print('inner_size = %gmm %gmm %gmm' % (PCB_hole_x,PCB_hole_y,PCB_z))
print('position = 0mm 0mm %gmm' % (z_increase))
print('orientation = 0 0 0')
print('material = g10')
print('role = "passive"\n')

chip_type = "faser"
silicon_z = 0.068
z_increase = z_increase + PCB_z/2. + silicon_z/2.
print('[Detector_%d_%d_%d]' % (0, 0, 0))
print('type = "%s"' % chip_type)
print('position = 0mm 0mm %gmm' % (z_increase))
print('orientation = 0deg 0deg 90deg\n')


# DUT 1
z_increase = z_increase + silicon_z/2. + d_airGap + tungsten_z/2.
print('[tungsten_%s]' % 1)
print('type = "box"')
print('size = %gmm %gmm %gmm' % (tungsten_x, tungsten_y, tungsten_z))
print('position = 0mm 10mm %gmm' % (z_increase))
print('orientation = 0 0 0')
print('material = tungsten')
print('role = "passive"\n')

z_increase = z_increase + tungsten_z/2. + copper_z/2. + d_W_PCB
print('[cooper_%s]' % 1)
print('type = "box"')
print('size = %gmm %gmm %gmm' % (copper_x, copper_y, copper_z))
print('inner_size = %gmm %gmm %gmm' % (copper_hole_x,copper_hole_y,copper_z))
print('position = 0mm 0mm %gmm' % (z_increase))
print('orientation = 0 0 0')
print('material = copper')
print('role = "passive"\n')

z_increase = z_increase + copper_z/2. + PCB_z/2.
print('[PCB_%s]' % 1)
print('type = "box"')
print('size = %gmm %gmm %gmm' % (PCB_x, PCB_y, PCB_z))
print('inner_size = %gmm %gmm %gmm' % (PCB_hole_x,PCB_hole_y,PCB_z))
print('position = 0mm 0mm %gmm' % (z_increase))
print('orientation = 0 0 0')
print('material = g10')
print('role = "passive"\n')


z_increase = z_increase + PCB_z/2. + silicon_z/2.
print('[Detector_%d_%d_%d]' % (2, 0, 0))
print('type = "%s"' % chip_type)
print('position = 0mm 0mm %gmm' % (z_increase))
print('orientation = 0deg 0deg 90deg\n')


# DUT 2
z_increase = z_increase + silicon_z/2. + d_airGap + tungsten_z/2.
print('[tungsten_%s]' % 2)
print('type = "box"')
print('size = %gmm %gmm %gmm' % (tungsten_x, tungsten_y, tungsten_z))
print('position = 0mm 10mm %gmm' % (z_increase))
print('orientation = 0 0 0')
print('material = tungsten')
print('role = "passive"\n')

z_increase = z_increase + tungsten_z/2. + copper_z/2. + d_W_PCB
print('[cooper_%s]' % 2)
print('type = "box"')
print('size = %gmm %gmm %gmm' % (copper_x, copper_y, copper_z))
print('inner_size = %gmm %gmm %gmm' % (copper_hole_x,copper_hole_y,copper_z))
print('position = 0mm 0mm %gmm' % (z_increase))
print('orientation = 0 0 0')
print('material = copper')
print('role = "passive"\n')

z_increase = z_increase + copper_z/2. + PCB_z/2.
print('[PCB_%s]' % 2)
print('type = "box"')
print('size = %gmm %gmm %gmm' % (PCB_x, PCB_y, PCB_z))
print('inner_size = %gmm %gmm %gmm' % (PCB_hole_x,PCB_hole_y,PCB_z))
print('position = 0mm 0mm %gmm' % (z_increase))
print('orientation = 0 0 0')
print('material = g10')
print('role = "passive"\n')

z_increase = z_increase + PCB_z/2. + silicon_z/2.
print('[Detector_%d_%d_%d]' % (4, 0, 0))
print('type = "%s"' % chip_type)
print('position = 0mm 0mm %gmm' % (z_increase))
print('orientation = 0deg 0deg 90deg\n')

'''
# FASER calorimeter
d_DUT2_calo = 100
pb_thick = 2
# in the testbeam 2022 there were 3 calorimeter modules next to each other
# each calo has a XY size of 12.12 cm
pb_x = 121.2*3
pb_y = 121.2
# 66 layers
pb_z = 66*pb_thick
z_increase = z_increase + silicon_z/2. + d_DUT2_calo
print('[caloPb_%s]' % 0)
print('type = "box"')
print('size = %gmm %gmm %gmm' % (pb_x, pb_y, pb_z))
print('position = 0mm 0mm %gmm' % (z_increase))
print('orientation = 0 0 0')
print('material = lead')
print('role = "passive"\n')
'''
