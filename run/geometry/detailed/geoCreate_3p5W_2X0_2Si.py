chip_type = "faser"

asic_size_y = 15.280
asic_size_x = 22.150

number_of_asics_y = 12
number_of_asics_x = 6
number_of_layers = 4

#tungsten_absorber_x = 200
#tungsten_absorber_y = 200
#tungsten_absorber_x = 194
#tungsten_absorber_y = 194
tungsten_absorber_x = 240
tungsten_absorber_y = 240
tungsten_absorber_z = 3.5

tungsten_absorber_2X0 = 3.5*2.

aluminum_cooling_x = 194
aluminum_cooling_y = 194
aluminum_cooling_z = 5

aluminum_module_x = 75.1*2
#aluminum_module_y = 31.2*3 + 25.2*3
aluminum_module_y = 31.2*6
aluminum_module_z = 6.5


#silicon_z = 0.150 # real one
#silicon_z = 0.068 # current one
silicon_z = 0.05
 
offset_x = asic_size_x * (number_of_asics_x - 1) / 2
offset_y = asic_size_y * (number_of_asics_y - 1) / 2

# full layer thickness = 3.5mm W +  5mm alu + 6.5mm alu + 0.15mm silicon + 15mm air
# full layer thickness = 3.5mm W +  5mm alu + 6.5mm alu + 0.068mm silicon + 15mm air  (in real life is 0.15mm silicon)
# pitch is 30mm, space needed for services is 30 - material = 30 - 3.5mm W +  5mm alu + 6.5mm alu + 0.068mm silicon
#distance_between_layers = 45.15
distance_between_layers = 30.
low_module_x = 132.72
low_module_y = 30.56
low_module_z = 3

low_module_pos_x = 66.36
low_module_pos_y = 15.28
low_module_pos_z = 8.25

high_module_x = 132.72
high_module_y = 25.2
high_module_z = 5.5

high_module_pos_x = 66.36
high_module_pos_y = 43.16
high_module_pos_z = 9.5

top_module_x = 132.72
top_module_y = 30.56
top_module_z = 1.

top_module_pos_x = 66.36
top_module_pos_y = 43.16
top_module_pos_z = 12.75

distance_between_low_module_y = 55.76
distance_between_high_module_y = 55.76
distance_between_top_module_y = 55.76

number_of_modules = 3

center_detector_x = 66.36
center_detector_y = 84.98

detector_pos_x = 11.06
detector_pos_y = 7.38
detector_pos_z = 9.775

distance_between_detector_x = 22.12
distance_between_detector_y = 12.08

# offset to have the first tungsten starting at z = 0
offset_z_origin = 13.25

sisi_gap = 15 # 15mm between silicon layers for services
# additional gap due to the 2 silicon layers on the first layers
additional_gap = sisi_gap + aluminum_cooling_z + high_module_z + top_module_z + silicon_z

# the position corresponds to the center of the volume
for layer in range(number_of_layers):
    # first layer with 2X0 and 2 Si
    if (layer == 0):
        print('[tungsten_%s]' % layer)
        print('type = "box"')
        print('size = %gmm %gmm %gmm' % (tungsten_absorber_x, tungsten_absorber_y, tungsten_absorber_2X0))
        print('position = %gmm %gmm %gmm' % (center_detector_x,center_detector_y,offset_z_origin + layer * distance_between_layers - 15.000 + tungsten_absorber_2X0/2.))
        print('orientation = 0 0 0')
        print('material = tungsten')
        print('role = "passive"\n')
        
        # first aluminum and silicon layer
        print('[alum_%s]' % layer)
        print('type = "box"')
        print('size = %gmm %gmm %gmm' % (aluminum_cooling_x, aluminum_cooling_y, aluminum_cooling_z))
        print('position = %gmm %gmm %gmm' % (center_detector_x,center_detector_y, offset_z_origin + layer * distance_between_layers - 15.000 + tungsten_absorber_2X0 + aluminum_cooling_z/2.))
        print('orientation = 0 0 0')
        print('material = aluminum')
        print('role = "passive"\n')

        for nModule in range(number_of_modules):
            # low module aluminum
            print('[simple_base_alum_%s_%s]' % (layer,nModule) )
            print('type = "box"')
            print('size = %gmm %gmm %gmm' % (low_module_x, low_module_y, low_module_z))
            print('position = %gmm %gmm %gmm' % (low_module_pos_x, low_module_pos_y + nModule*distance_between_low_module_y, offset_z_origin + layer * distance_between_layers - 15.000 + tungsten_absorber_2X0 + aluminum_cooling_z + low_module_z/2.))
            print('orientation = 0 0 0')
            print('material = aluminum')
            print('role = "passive"\n')

            # high module aluminum
            print('[sec1_base_alum_%s_%s]' % (layer,nModule) )
            print('type = "box"')
            print('size = %gmm %gmm %gmm' % (high_module_x, high_module_y, high_module_z))
            print('position = %gmm %gmm %gmm' % (high_module_pos_x, high_module_pos_y + nModule*distance_between_high_module_y, offset_z_origin + layer * distance_between_layers - 15.000 + tungsten_absorber_2X0 + aluminum_cooling_z + high_module_z/2.))
            print('orientation = 0 0 0')
            print('material = aluminum')
            print('role = "passive"\n')

            # top module aluminum
            print('[sec2_base_alum_%s_%s]' % (layer,nModule) )
            print('type = "box"')
            print('size = %gmm %gmm %gmm' % (top_module_x, top_module_y, top_module_z))
            print('position = %gmm %gmm %gmm' % (top_module_pos_x, top_module_pos_y + nModule*distance_between_top_module_y, offset_z_origin + layer * distance_between_layers - 15.000 + tungsten_absorber_2X0 + aluminum_cooling_z + high_module_z + top_module_z/2.))
            print('orientation = 0 0 0')
            print('material = aluminum')
            print('role = "passive"\n')

        for asic_y in range(number_of_asics_y):
            for asic_x in range(number_of_asics_x):
                print('[Detector_%d_%d_%d]' % (layer, asic_x, asic_y))
                print('type = "%s"' % chip_type)
                if (asic_y % 2 == 0):
                    distance_between_detector_y = 13.94
                    #distance_between_detector_y = 12.08
                elif (asic_y == 1):
                    distance_between_detector_y = 15.80
                elif (asic_y == 3):
                    distance_between_detector_y = 14.56
                elif (asic_y == 5):
                    distance_between_detector_y = 14.312
                elif (asic_y == 7):
                    distance_between_detector_y = 14.205714
                elif (asic_y == 9):
                    distance_between_detector_y = 14.146667
                elif (asic_y == 11):
                    distance_between_detector_y = 14.109091
                    # low modules
                if (asic_y == 0 or asic_y == 1 or asic_y == 4 or asic_y == 5 or asic_y == 8 or asic_y == 9):
                    print('position = %gmm %gmm %gmm' % (detector_pos_x + (asic_x)*distance_between_detector_x, detector_pos_y + (asic_y)*distance_between_detector_y, offset_z_origin + layer * distance_between_layers - 15.000 + tungsten_absorber_2X0 + aluminum_cooling_z + low_module_z + silicon_z/2.  ))
                    # high modules
                if (asic_y == 2 or asic_y == 3 or asic_y == 6 or asic_y == 7 or asic_y == 10 or asic_y == 11):
                    print('position = %gmm %gmm %gmm' % (detector_pos_x + (asic_x)*distance_between_detector_x, detector_pos_y + (asic_y)*distance_between_detector_y, offset_z_origin + layer * distance_between_layers - 15.000 + tungsten_absorber_2X0 + aluminum_cooling_z + high_module_z + top_module_z + silicon_z/2. ))
                print('orientation = 0deg 0deg %ddeg\n' % (0 if asic_y % 2 else 180))



        # 2nd Aluminum + silicon layer
        print('[alum_%s]' % str(layer+1))
        print('type = "box"')
        print('size = %gmm %gmm %gmm' % (aluminum_cooling_x, aluminum_cooling_y, aluminum_cooling_z))
        print('position = %gmm %gmm %gmm' % (center_detector_x,center_detector_y, offset_z_origin + layer * distance_between_layers - 15.000 + tungsten_absorber_2X0 + aluminum_cooling_z + high_module_z + top_module_z + silicon_z + sisi_gap + aluminum_cooling_z/2.))
        print('orientation = 0 0 0')
        print('material = aluminum')
        print('role = "passive"\n')

        for nModule in range(number_of_modules):
            # low module aluminum
            print('[simple_base_alum_%s_%s]' % (str(layer+1),nModule) )
            print('type = "box"')
            print('size = %gmm %gmm %gmm' % (low_module_x, low_module_y, low_module_z))
            print('position = %gmm %gmm %gmm' % (low_module_pos_x, low_module_pos_y + nModule*distance_between_low_module_y, offset_z_origin + layer * distance_between_layers - 15.000 + tungsten_absorber_2X0 + aluminum_cooling_z + high_module_z + top_module_z + silicon_z + sisi_gap + aluminum_cooling_z + low_module_z/2.))
            print('orientation = 0 0 0')
            print('material = aluminum')
            print('role = "passive"\n')

            # high module aluminum
            print('[sec1_base_alum_%s_%s]' % (str(layer+1),nModule) )
            print('type = "box"')
            print('size = %gmm %gmm %gmm' % (high_module_x, high_module_y, high_module_z))
            print('position = %gmm %gmm %gmm' % (high_module_pos_x, high_module_pos_y + nModule*distance_between_high_module_y, offset_z_origin + layer * distance_between_layers - 15.000 + tungsten_absorber_2X0 + aluminum_cooling_z + high_module_z + top_module_z + silicon_z + sisi_gap + aluminum_cooling_z + high_module_z/2.))
            print('orientation = 0 0 0')
            print('material = aluminum')
            print('role = "passive"\n')

            # top module aluminum
            print('[sec2_base_alum_%s_%s]' % (str(layer+1),nModule) )
            print('type = "box"')
            print('size = %gmm %gmm %gmm' % (top_module_x, top_module_y, top_module_z))
            print('position = %gmm %gmm %gmm' % (top_module_pos_x, top_module_pos_y + nModule*distance_between_top_module_y, offset_z_origin + layer * distance_between_layers - 15.000 + tungsten_absorber_2X0 + aluminum_cooling_z + high_module_z + top_module_z + silicon_z + sisi_gap + aluminum_cooling_z + high_module_z + top_module_z/2.))
            print('orientation = 0 0 0')
            print('material = aluminum')
            print('role = "passive"\n')
            
        for asic_y in range(number_of_asics_y):
            for asic_x in range(number_of_asics_x):
                print('[Detector_%d_%d_%d]' % (layer + 1, asic_x, asic_y))
                print('type = "%s"' % chip_type)
                if (asic_y % 2 == 0):
                    distance_between_detector_y = 13.94
                    #distance_between_detector_y = 12.08
                elif (asic_y == 1):
                    distance_between_detector_y = 15.80
                elif (asic_y == 3):
                    distance_between_detector_y = 14.56
                elif (asic_y == 5):
                    distance_between_detector_y = 14.312
                elif (asic_y == 7):
                    distance_between_detector_y = 14.205714
                elif (asic_y == 9):
                    distance_between_detector_y = 14.146667
                elif (asic_y == 11):
                    distance_between_detector_y = 14.109091
                    # low modules
                if (asic_y == 0 or asic_y == 1 or asic_y == 4 or asic_y == 5 or asic_y == 8 or asic_y == 9):
                    print('position = %gmm %gmm %gmm' % (detector_pos_x + (asic_x)*distance_between_detector_x, detector_pos_y + (asic_y)*distance_between_detector_y, offset_z_origin + layer * distance_between_layers - 15.000 + tungsten_absorber_2X0 + aluminum_cooling_z + high_module_z + top_module_z + silicon_z + sisi_gap + aluminum_cooling_z +  low_module_z + silicon_z/2.  ))
                    # high modules
                if (asic_y == 2 or asic_y == 3 or asic_y == 6 or asic_y == 7 or asic_y == 10 or asic_y == 11):
                    print('position = %gmm %gmm %gmm' % (detector_pos_x + (asic_x)*distance_between_detector_x, detector_pos_y + (asic_y)*distance_between_detector_y, offset_z_origin + layer * distance_between_layers - 15.000 + tungsten_absorber_2X0 + aluminum_cooling_z + high_module_z + top_module_z + silicon_z + sisi_gap + aluminum_cooling_z + high_module_z + top_module_z + silicon_z/2. ))
                print('orientation = 0deg 0deg %ddeg\n' % (0 if asic_y % 2 else 180))


    # 2nd layer with 2X0 and 2Si
    elif (layer == 1):
        print('[tungsten_%s]' % layer)
        print('type = "box"')
        print('size = %gmm %gmm %gmm' % (tungsten_absorber_x, tungsten_absorber_y, tungsten_absorber_2X0))
        print('position = %gmm %gmm %gmm' % (center_detector_x,center_detector_y,offset_z_origin + layer * distance_between_layers - 15.000 + additional_gap + tungsten_absorber_z + tungsten_absorber_2X0/2.))
        print('orientation = 0 0 0')
        print('material = tungsten')
        print('role = "passive"\n')
        
        # first aluminum and silicon layer
        print('[alum_%s]' % str(layer+1))
        print('type = "box"')
        print('size = %gmm %gmm %gmm' % (aluminum_cooling_x, aluminum_cooling_y, aluminum_cooling_z))
        print('position = %gmm %gmm %gmm' % (center_detector_x,center_detector_y, offset_z_origin + layer * distance_between_layers - 15.000 + additional_gap + tungsten_absorber_z + tungsten_absorber_2X0 + aluminum_cooling_z/2.))
        print('orientation = 0 0 0')
        print('material = aluminum')
        print('role = "passive"\n')

        for nModule in range(number_of_modules):
            # low module aluminum
            print('[simple_base_alum_%s_%s]' % (str(layer+1),nModule) )
            print('type = "box"')
            print('size = %gmm %gmm %gmm' % (low_module_x, low_module_y, low_module_z))
            print('position = %gmm %gmm %gmm' % (low_module_pos_x, low_module_pos_y + nModule*distance_between_low_module_y, offset_z_origin + layer * distance_between_layers - 15.000 + additional_gap + tungsten_absorber_z + tungsten_absorber_2X0 + aluminum_cooling_z + low_module_z/2.))
            print('orientation = 0 0 0')
            print('material = aluminum')
            print('role = "passive"\n')

            # high module aluminum
            print('[sec1_base_alum_%s_%s]' % (str(layer+1),nModule) )
            print('type = "box"')
            print('size = %gmm %gmm %gmm' % (high_module_x, high_module_y, high_module_z))
            print('position = %gmm %gmm %gmm' % (high_module_pos_x, high_module_pos_y + nModule*distance_between_high_module_y, offset_z_origin + layer * distance_between_layers - 15.000 + additional_gap + tungsten_absorber_2X0 + tungsten_absorber_z + aluminum_cooling_z + high_module_z/2.))
            print('orientation = 0 0 0')
            print('material = aluminum')
            print('role = "passive"\n')

            # top module aluminum
            print('[sec2_base_alum_%s_%s]' % (str(layer+1),nModule) )
            print('type = "box"')
            print('size = %gmm %gmm %gmm' % (top_module_x, top_module_y, top_module_z))
            print('position = %gmm %gmm %gmm' % (top_module_pos_x, top_module_pos_y + nModule*distance_between_top_module_y, offset_z_origin + layer * distance_between_layers - 15.000 + additional_gap + tungsten_absorber_2X0 + tungsten_absorber_z + aluminum_cooling_z + high_module_z + top_module_z/2.))
            print('orientation = 0 0 0')
            print('material = aluminum')
            print('role = "passive"\n')

        for asic_y in range(number_of_asics_y):
            for asic_x in range(number_of_asics_x):
                print('[Detector_%d_%d_%d]' % (layer + 1, asic_x, asic_y))
                print('type = "%s"' % chip_type)
                if (asic_y % 2 == 0):
                    distance_between_detector_y = 13.94
                    #distance_between_detector_y = 12.08
                elif (asic_y == 1):
                    distance_between_detector_y = 15.80
                elif (asic_y == 3):
                    distance_between_detector_y = 14.56
                elif (asic_y == 5):
                    distance_between_detector_y = 14.312
                elif (asic_y == 7):
                    distance_between_detector_y = 14.205714
                elif (asic_y == 9):
                    distance_between_detector_y = 14.146667
                elif (asic_y == 11):
                    distance_between_detector_y = 14.109091
                    # low modules
                if (asic_y == 0 or asic_y == 1 or asic_y == 4 or asic_y == 5 or asic_y == 8 or asic_y == 9):
                    print('position = %gmm %gmm %gmm' % (detector_pos_x + (asic_x)*distance_between_detector_x, detector_pos_y + (asic_y)*distance_between_detector_y, offset_z_origin + layer * distance_between_layers - 15.000 + additional_gap + tungsten_absorber_2X0 + tungsten_absorber_z + aluminum_cooling_z + low_module_z + silicon_z/2.  ))
                # high modules
                if (asic_y == 2 or asic_y == 3 or asic_y == 6 or asic_y == 7 or asic_y == 10 or asic_y == 11):
                    print('position = %gmm %gmm %gmm' % (detector_pos_x + (asic_x)*distance_between_detector_x, detector_pos_y + (asic_y)*distance_between_detector_y, offset_z_origin + layer * distance_between_layers - 15.000 + additional_gap + tungsten_absorber_2X0 + tungsten_absorber_z + aluminum_cooling_z + high_module_z + top_module_z + silicon_z/2. ))
                print('orientation = 0deg 0deg %ddeg\n' % (0 if asic_y % 2 else 180))



        # 2nd Aluminum + silicon layer
        print('[alum_%s]' % str(layer+2))
        print('type = "box"')
        print('size = %gmm %gmm %gmm' % (aluminum_cooling_x, aluminum_cooling_y, aluminum_cooling_z))
        print('position = %gmm %gmm %gmm' % (center_detector_x,center_detector_y, offset_z_origin + layer * distance_between_layers - 15.000 + additional_gap + tungsten_absorber_2X0 + tungsten_absorber_z + aluminum_cooling_z + high_module_z + top_module_z + sisi_gap + aluminum_cooling_z/2.))
        print('orientation = 0 0 0')
        print('material = aluminum')
        print('role = "passive"\n')

        for nModule in range(number_of_modules):
            # low module aluminum
            print('[simple_base_alum_%s_%s]' % (str(layer+2),nModule) )
            print('type = "box"')
            print('size = %gmm %gmm %gmm' % (low_module_x, low_module_y, low_module_z))
            print('position = %gmm %gmm %gmm' % (low_module_pos_x, low_module_pos_y + nModule*distance_between_low_module_y, offset_z_origin + layer * distance_between_layers - 15.000 + additional_gap + tungsten_absorber_2X0 + tungsten_absorber_z + aluminum_cooling_z + high_module_z + top_module_z + sisi_gap + aluminum_cooling_z + low_module_z/2.))
            print('orientation = 0 0 0')
            print('material = aluminum')
            print('role = "passive"\n')

            # high module aluminum
            print('[sec1_base_alum_%s_%s]' % (str(layer+2),nModule) )
            print('type = "box"')
            print('size = %gmm %gmm %gmm' % (high_module_x, high_module_y, high_module_z))
            print('position = %gmm %gmm %gmm' % (high_module_pos_x, high_module_pos_y + nModule*distance_between_high_module_y, offset_z_origin + layer * distance_between_layers - 15.000 + additional_gap + tungsten_absorber_2X0 + tungsten_absorber_z + aluminum_cooling_z + high_module_z + top_module_z + sisi_gap + aluminum_cooling_z + high_module_z/2.))
            print('orientation = 0 0 0')
            print('material = aluminum')
            print('role = "passive"\n')

            # top module aluminum
            print('[sec2_base_alum_%s_%s]' % (str(layer+2),nModule) )
            print('type = "box"')
            print('size = %gmm %gmm %gmm' % (top_module_x, top_module_y, top_module_z))
            print('position = %gmm %gmm %gmm' % (top_module_pos_x, top_module_pos_y + nModule*distance_between_top_module_y, offset_z_origin + layer * distance_between_layers - 15.000 + additional_gap + tungsten_absorber_2X0 + tungsten_absorber_z + aluminum_cooling_z + high_module_z + top_module_z + sisi_gap + aluminum_cooling_z + high_module_z + top_module_z/2.))
            print('orientation = 0 0 0')
            print('material = aluminum')
            print('role = "passive"\n')

        for asic_y in range(number_of_asics_y):
            for asic_x in range(number_of_asics_x):
                print('[Detector_%d_%d_%d]' % (layer + 2, asic_x, asic_y))
                print('type = "%s"' % chip_type)
                if (asic_y % 2 == 0):
                    distance_between_detector_y = 13.94
                    #distance_between_detector_y = 12.08
                elif (asic_y == 1):
                    distance_between_detector_y = 15.80
                elif (asic_y == 3):
                    distance_between_detector_y = 14.56
                elif (asic_y == 5):
                    distance_between_detector_y = 14.312
                elif (asic_y == 7):
                    distance_between_detector_y = 14.205714
                elif (asic_y == 9):
                    distance_between_detector_y = 14.146667
                elif (asic_y == 11):
                    distance_between_detector_y = 14.109091
                    # low modules
                if (asic_y == 0 or asic_y == 1 or asic_y == 4 or asic_y == 5 or asic_y == 8 or asic_y == 9):
                    print('position = %gmm %gmm %gmm' % (detector_pos_x + (asic_x)*distance_between_detector_x, detector_pos_y + (asic_y)*distance_between_detector_y, offset_z_origin + layer * distance_between_layers - 15.000 + additional_gap + tungsten_absorber_2X0 + tungsten_absorber_z + aluminum_cooling_z + high_module_z + top_module_z + sisi_gap + aluminum_cooling_z + low_module_z + silicon_z/2.  ))
                # high modules
                if (asic_y == 2 or asic_y == 3 or asic_y == 6 or asic_y == 7 or asic_y == 10 or asic_y == 11):
                    print('position = %gmm %gmm %gmm' % (detector_pos_x + (asic_x)*distance_between_detector_x, detector_pos_y + (asic_y)*distance_between_detector_y, offset_z_origin + layer * distance_between_layers - 15.000 + additional_gap + tungsten_absorber_2X0 + tungsten_absorber_z + aluminum_cooling_z + high_module_z + top_module_z + sisi_gap + aluminum_cooling_z + high_module_z + top_module_z + silicon_z/2. ))
                print('orientation = 0deg 0deg %ddeg\n' % (0 if asic_y % 2 else 180))
    # rest of layers
    else:
        print('[tungsten_%s]' % layer)
        print('type = "box"')
        print('size = %gmm %gmm %gmm' % (tungsten_absorber_x, tungsten_absorber_y, tungsten_absorber_z))
        print('position = %gmm %gmm %gmm' % (center_detector_x,center_detector_y, offset_z_origin + tungsten_absorber_2X0 + layer * distance_between_layers - 15.000 + 2.*additional_gap + tungsten_absorber_z/2.))
        print('orientation = 0 0 0')
        print('material = tungsten')
        print('role = "passive"\n')
        
        # first aluminum and silicon layer
        print('[alum_%s]' % str(layer+2))
        print('type = "box"')
        print('size = %gmm %gmm %gmm' % (aluminum_cooling_x, aluminum_cooling_y, aluminum_cooling_z))
        print('position = %gmm %gmm %gmm' % (center_detector_x,center_detector_y, offset_z_origin + tungsten_absorber_2X0 + layer * distance_between_layers - 15.000 + 2.*additional_gap + tungsten_absorber_z + aluminum_cooling_z/2.))
        print('orientation = 0 0 0')
        print('material = aluminum')
        print('role = "passive"\n')

        for nModule in range(number_of_modules):
            # low module aluminum
            print('[simple_base_alum_%s_%s]' % (str(layer+2),nModule) )
            print('type = "box"')
            print('size = %gmm %gmm %gmm' % (low_module_x, low_module_y, low_module_z))
            print('position = %gmm %gmm %gmm' % (low_module_pos_x, low_module_pos_y + nModule*distance_between_low_module_y, offset_z_origin + tungsten_absorber_2X0 + layer * distance_between_layers - 15.000 + 2.*additional_gap + tungsten_absorber_z + aluminum_cooling_z + low_module_z/2.))
            print('orientation = 0 0 0')
            print('material = aluminum')
            print('role = "passive"\n')

            # high module aluminum
            print('[sec1_base_alum_%s_%s]' % (str(layer+2),nModule) )
            print('type = "box"')
            print('size = %gmm %gmm %gmm' % (high_module_x, high_module_y, high_module_z))
            print('position = %gmm %gmm %gmm' % (high_module_pos_x, high_module_pos_y + nModule*distance_between_high_module_y, offset_z_origin + tungsten_absorber_2X0 + layer * distance_between_layers - 15.000 + 2.*additional_gap + tungsten_absorber_z + aluminum_cooling_z + high_module_z/2.))
            print('orientation = 0 0 0')
            print('material = aluminum')
            print('role = "passive"\n')

            # top module aluminum
            print('[sec2_base_alum_%s_%s]' % (str(layer+2),nModule) )
            print('type = "box"')
            print('size = %gmm %gmm %gmm' % (top_module_x, top_module_y, top_module_z))
            print('position = %gmm %gmm %gmm' % (top_module_pos_x, top_module_pos_y + nModule*distance_between_top_module_y, offset_z_origin + tungsten_absorber_2X0 + layer * distance_between_layers - 15.000 + 2.*additional_gap + tungsten_absorber_z + aluminum_cooling_z + high_module_z + top_module_z/2.))
            print('orientation = 0 0 0')
            print('material = aluminum')
            print('role = "passive"\n')

        for asic_y in range(number_of_asics_y):
            for asic_x in range(number_of_asics_x):
                print('[Detector_%d_%d_%d]' % (layer + 2, asic_x, asic_y))
                print('type = "%s"' % chip_type)
                if (asic_y % 2 == 0):
                    distance_between_detector_y = 13.94
                    #distance_between_detector_y = 12.08
                elif (asic_y == 1):
                    distance_between_detector_y = 15.80
                elif (asic_y == 3):
                    distance_between_detector_y = 14.56
                elif (asic_y == 5):
                    distance_between_detector_y = 14.312
                elif (asic_y == 7):
                    distance_between_detector_y = 14.205714
                elif (asic_y == 9):
                    distance_between_detector_y = 14.146667
                elif (asic_y == 11):
                    distance_between_detector_y = 14.109091
                    # low modules
                if (asic_y == 0 or asic_y == 1 or asic_y == 4 or asic_y == 5 or asic_y == 8 or asic_y == 9):
                    print('position = %gmm %gmm %gmm' % (detector_pos_x + (asic_x)*distance_between_detector_x, detector_pos_y + (asic_y)*distance_between_detector_y, offset_z_origin + tungsten_absorber_2X0 + layer * distance_between_layers - 15.000 + 2.*additional_gap + tungsten_absorber_z + aluminum_cooling_z + low_module_z + silicon_z/2.  ))
                    # high modules
                if (asic_y == 2 or asic_y == 3 or asic_y == 6 or asic_y == 7 or asic_y == 10 or asic_y == 11):
                    print('position = %gmm %gmm %gmm' % (detector_pos_x + (asic_x)*distance_between_detector_x, detector_pos_y + (asic_y)*distance_between_detector_y, offset_z_origin + tungsten_absorber_2X0 + layer * distance_between_layers - 15.000 + 2.*additional_gap + tungsten_absorber_z + aluminum_cooling_z + high_module_z + top_module_z + silicon_z/2. ))
                print('orientation = 0deg 0deg %ddeg\n' % (0 if asic_y % 2 else 180))
