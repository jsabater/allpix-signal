chip_type = "faser"

asic_size_y = 15.280
asic_size_x = 22.150

number_of_asics_y = 12
number_of_asics_x = 6
number_of_layers = 6

tungsten_absorber_x = 200
tungsten_absorber_y = 200
tungsten_absorber_z = 3.5

aluminum_cooling_x = 194
aluminum_cooling_y = 194
aluminum_cooling_z = 5

aluminum_module_x = 75.1*2
#aluminum_module_y = 31.2*3 + 25.2*3
aluminum_module_y = 31.2*6
aluminum_module_z = 6.5


#silicon_z = 0.150 # real one
silicon_z = 0.068 # current one
 
offset_x = asic_size_x * (number_of_asics_x - 1) / 2
offset_y = asic_size_y * (number_of_asics_y - 1) / 2

# full layer thickness = 3.5mm W +  5mm alu + 6.5mm alu + 0.15mm silicon + 15mm air
# full layer thickness = 3.5mm W +  5mm alu + 6.5mm alu + 0.068mm silicon + 15mm air  (in real life is 0.15mm silicon)
# pitch is 30mm, space needed for services is 30 - material = 30 - 3.5mm W +  5mm alu + 6.5mm alu + 0.068mm silicon
#distance_between_layers = 45.15
distance_between_layers = 32.5
# air extra that we have to add to keep the amoutn of air (15mm) the same between planes
extra_air = 0
# the position corresponds to the center of the volume
for layer in range(number_of_layers):
    if layer == 0 or layer == 1:
        tungsten_absorber_z = 6
    else:
        tungsten_absorber_z = 2
    
    if layer == 3:
        extra_air = -4.
    if layer == 4:
        extra_air = -8.
    if layer == 5:
        extra_air = -12.


    print('[tungsten_%s]' % layer)
    print('type = "box"')
    print('size = %gmm %gmm %gmm' % (tungsten_absorber_x, tungsten_absorber_y, tungsten_absorber_z))
    print('position = 0mm 0mm %gmm' % (layer * distance_between_layers - 15.000 + tungsten_absorber_z/2. + extra_air))
    print('orientation = 0 0 0')
    print('material = tungsten')
    print('role = "passive"\n')

    print('[aluminum_cooling_%s]' % layer)
    print('type = "box"')
    print('size = %gmm %gmm %gmm' % (aluminum_cooling_x, aluminum_cooling_y, aluminum_cooling_z))
    print('position = 0mm 0mm %gmm' % (layer * distance_between_layers - 15.000 + tungsten_absorber_z + aluminum_cooling_z/2. + extra_air))
    print('orientation = 0 0 0')
    print('material = aluminum')
    print('role = "passive"\n')

    print('[aluminum_module_%s]' % layer)
    print('type = "box"')
    print('size = %gmm %gmm %gmm' % (aluminum_module_x, aluminum_module_y, aluminum_module_z))
    print('position = 0mm 0mm %gmm' % (layer * distance_between_layers - 15.000 + tungsten_absorber_z + aluminum_cooling_z + aluminum_module_z/2. + extra_air))
    print('orientation = 0 0 0')
    print('material = aluminum')
    print('role = "passive"\n')

    for asic_x in range(number_of_asics_x):
        for asic_y in range(number_of_asics_y):
            print('[Detector_%d_%d_%d]' % (layer + 1, asic_x + 1, asic_y + 1))
            print('type = "%s"' % chip_type)
            pos_x = asic_x * asic_size_x - offset_x 
            pos_y = asic_y * asic_size_y - offset_y - (0.46 if asic_x % 2 else 0)
            print('position = %gmm %gmm %gmm' % (pos_x, pos_y, layer * distance_between_layers - 15.000 + tungsten_absorber_z + aluminum_cooling_z + aluminum_module_z + silicon_z/2. + extra_air ))
            print('orientation = 0deg 0deg %ddeg\n' % (180 if asic_x % 2 else 0))

#print('offset x ',offset_x)
#print('offset y ',offset_y)
