#include "CalibrationUtils.hpp"

#include <TGraph.h>
#include <TF1.h>

#include <cmath>
#include <sstream>

using namespace allpix::internal;

double FittedFunction::operator()(double x) const {
    return x < x_offset ? 1200.0 : a + b * (1 - std::exp(c * (x - x_offset)));
}

Bucket readSets(std::ifstream& file, const size_t num_sets, const size_t num_values, double mult = 1) {
    Bucket bucket(num_sets);

    for (auto& set : bucket) {
        set.resize(num_values);
        for (auto& value : set) {
            file >> value;
            value *= mult;
        }
    }

    return bucket;
}

std::pair<Bucket, Bucket> allpix::internal::readBuckets(std::ifstream& file) {

    size_t num_sets, num_values;

    file >> num_sets >> num_values;

    return {
        readSets(file, num_sets, num_values),
        readSets(file, num_sets, num_values, 1000)
    };
}

FittedFunction allpix::internal::create_function_fitting(const std::vector<double>& A, const std::vector<double>& B, double x_offset) {
    TGraph g(A.size(), A.data(), B.data());
    //    std::cout << "----------------\n";
    //for (int i = 0; i < A.size(); ++i) {
    //        std::cout << A[i] << " -> " << B[i] << '\n';
    //    }
    std::stringstream equation;

    equation <<  "x<" << x_offset << "? 1200.0 : [0]+[1]*(1-exp([2]*(x-" << x_offset << ")))";
    //g.SetPointStyle(8);
    TF1 p1("p1",equation.str().c_str(),0,200);
    g.Fit("p1","WQN");
    p1.SetParameters(1200.0, -430.0, -0.09);
    g.Fit("p1","Q","",0,200);
    //g.Draw("AP");
    // printf("%f+%f*exp(%f*x)\n", p1.GetParameter(0), p1.GetParameter(1), p1.GetParameter(2));
    return {
        p1.GetParameter(0),
        p1.GetParameter(1),
        p1.GetParameter(2),
        x_offset
    };
}

// std::ostream& operator<<(std::ostream& os, const allpix::internal::FittedFunction& f) {
//     os << "Fitted function: f(x) = " << f.a << " + " << f.b << " * exp(" << f.c << " * x)\n"
//        << "Inverse function: f(x) = log((x - " << f.a << ") / " << f.b << ") / " << f.c;
//     return os;
// }

std::function<double (double)> allpix::internal::create_binning_function(double from, double to, double offset) {
    return [=](double value){
        if (value < from) {
            return from + offset / 2;
        }
        if (value > to) {
            return to - offset / 2;
        }
        auto bin_index = std::floor((value - from) / offset);
        return from + bin_index * offset + offset / 2;
    };
}

std::function<double (double)> allpix::internal::create_inverse(FittedFunction& f) {
    return [=](double x) { 
        return x < f.a ? ((std::log(-(- f.a - f.b + x) / f.b) + (f.c * f.x_offset)) / f.c) : f.x_offset;
    };
}
