#ifndef ALLPIX_CALIBRATION_UTILS_H
#define ALLPIX_CALIBRATION_UTILS_H

#include <vector>
#include <fstream>
#include <utility>
#include <functional>
#include <ostream>

namespace allpix::internal {
    using Bucket = std::vector<std::vector<double>>;

    struct FittedFunction {
        double a, b, c, x_offset;

        double operator()(double x) const;
    };

    std::pair<Bucket, Bucket> readBuckets(std::ifstream& file);

    FittedFunction create_function_fitting(const std::vector<double>& A, const std::vector<double>& B, double x_offset);

    std::function<double (double)> create_binning_function(double from, double to, double offset);

    std::function<double (double)> create_inverse(FittedFunction& f);
}

// std::ostream& operator<<(std::ostream& os, const allpix::internal::FittedFunction& f);

#endif