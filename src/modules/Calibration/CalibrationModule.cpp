/**
 * @file
 * @brief Implementation of [Calibration] module
 *
 * @copyright Copyright (c) 2017-2022 CERN and the Allpix Squared authors.
 * This software is distributed under the terms of the MIT License, copied verbatim in the file "LICENSE.md".
 * In applying this license, CERN does not waive the privileges and immunities granted to it by virtue of its status as an
 * Intergovernmental Organization or submit itself to any jurisdiction.
 */

#include "CalibrationModule.hpp"

#include <algorithm>
#include <random>
#include <string>
#include <utility>

#include "core/utils/log.h"

#include "objects/SimplifiedHit.hpp"

// #define PRINT_CSV_INFO

using namespace allpix;

constexpr double FC_IN_EL_CHARGE = 0.00016021766208;

std::ostream& yy(std::ostream& os, const internal::FittedFunction f) {
    char buffer[10000];
    // x<[2] ? 1.2 : [3]+[0]*(1-exp([1]*(x-[2])))
    // os << "Fitted function: f(x) = " << f.a << " + " << f.b << " * exp(" << f.c << " * x)\n"
    //    << "Inverse function: f(x) = log((x - " << f.a << ") / " << f.b << ") / " << f.c;
    std::sprintf(buffer,
                 "Fitted function: f(x) = x < %f ? 1200 : %f + %f *(1 - exp(%f * (x - %f)))\nInverse: x < 1200.0 ? "
                 "(std::log(-(- %f - %f + x) / %f) / %f + %f) : %f\n",
                 f.x_offset,
                 f.a,
                 f.b,
                 f.c,
                 f.x_offset,
                 f.a,
                 f.b,
                 f.b,
                 f.c,
                 f.x_offset,
                 f.x_offset);
    os << buffer;
    return os;
}

CalibrationModule::CalibrationModule(Configuration& config, Messenger* messenger, GeometryManager* geo_manager)
    : SequentialModule(config), geo_manager_(geo_manager), messenger_(messenger), bin_function_(), random_engine_(),
      normal_distribution_(), bin_step_{} {
    // Enable multithreading of this module if multithreading is enabled
    allow_multithreading();

    assert(messenger && "messenger must be non-null");
    assert(geo_manager && "geo_mgr must be non-null");

    // ... Implement ... (Typically bounds the required messages and optionally sets configuration defaults)
    // Input required by this module
    messenger_->bindMulti<PixelChargeMessage>(this, MsgFlags::REQUIRED);

    config_.setDefault("reference_data", "reference.data");
}

static double calculate_x_offset(const std::vector<double>& q, const std::vector<double>& v) {
    std::size_t i = 1;
    for(; v.size() > i; ++i) {
        if(v[i - 1] != v[i])
            break;
    }

    return i == 1 ? 0.0 : ((q[i - 1] + q[i]) / 2.0);
}

void CalibrationModule::initialize() {

    // TODO: bin_to should probably be equal to saturation value (before x_offset)
    auto bin_to = config_.get<double>("bin_to");

    auto mean = config_.get<double>("mean");
    auto stddev = config_.get<double>("stddev");
    normal_distribution_ = std::normal_distribution<double>(mean, stddev);

    // Open the file with the objects
    auto file_path = config_.getPathWithExtension("reference_data", "data", true);
    std::ifstream input_file(file_path);
    if(!input_file.is_open()) {
        throw InvalidValueError(config_, "file_name", "could not open input file");
    }

    auto [q, v] = internal::readBuckets(input_file);
    auto size = v.size();

    functions_.reserve(size);
    inverse_functions_.reserve(size);

    auto min_bound = std::numeric_limits<double>::infinity();

    for(auto i = 0; i < size; ++i) {
        auto x_offset = calculate_x_offset(q[i], v[i]);
        LOG(DEBUG) << "x offset: " << x_offset;
        auto f = internal::create_function_fitting(q[i], v[i], x_offset);
        min_bound = std::min(min_bound, f.a + f.b);
        functions_.emplace_back(std::move(f));

        auto inverse = internal::create_inverse(f);
        LOG(DEBUG) << inverse(1188.25) << " :: " << inverse(f(0.8)) << " == " << 0.8;
        inverse_functions_.emplace_back(std::move(inverse));
    }

    bin_step_ = (bin_to - min_bound) / 16;
    bin_function_ = internal::create_binning_function(min_bound, bin_to, bin_step_);

    // Sanity check
    LOG(DEBUG) << "first bin: " << (*bin_function_)(std::numeric_limits<double>::infinity());

    uniform_distribution_ = std::uniform_int_distribution<int>(0, size - 1); //(inclusive, inclusive)
}

std::tuple<int, const internal::FittedFunction&, const std::function<double(double)>&>
CalibrationModule::get_random_function() {
    auto index = (*uniform_distribution_)(random_engine_);

    return {index, functions_[index], inverse_functions_[index]};
}

void CalibrationModule::run(Event* event) {
    auto messages = messenger_->fetchMultiMessage<PixelChargeMessage>(this, event);
    // ... Implement ... (Typically uses the configuration to execute function and outputs an message)
    // Loop through all received messages and print some information

    for(auto& message : messages) {
        std::string detectorName = message->getDetector()->getName();
        LOG(DEBUG) << "Picked up " << message->getData().size() << " objects from detector " << detectorName;

        std::vector<SimplifiedHit> hits;

        for(auto& hit : message->getData()) {
            auto signal = hit.getAbsoluteCharge() * FC_IN_EL_CHARGE;

            auto index = hit.getPixel().getIndex();
            auto global_center = hit.getPixel().getGlobalCenter();
            auto [idx, f, inverse_f] = get_random_function();
            auto noise = (*normal_distribution_)(random_engine_);
            auto Vn = f(signal) + noise;
            auto binned = (*bin_function_)(Vn);

            while(f.a + f.b > binned) {
                binned += bin_step_;
            }
            auto new_signal = std::abs(binned - first_bin_) < std::numeric_limits<double>::epsilon() ? 0 : inverse_f(binned);
	    
	    //////////////////////////////////////////////////////////////////////////////////////////
	    ////////////////////// Temporary hand-made discretization of charge //////////////////////
	    //////////////////////////////////////////////////////////////////////////////////////////
	    // 16 logarithmic bins between 1 and 65 fC
	    std::vector<double> q_bins{1.0, 1.32087, 1.7447, 2.30453, 3.04399, 4.02073, 5.31087, 7.01498, 9.26589, 12.2391, 16.1662, 21.3535, 28.2053, 37.2556, 49.2099, 65.0};
	    
	    
	    // Input number
	    double input_charge = signal;
	    int bin_index = -1;  // To store the index of the bin
	    int num_bins = q_bins.size();

	    // if it's above 65fC assign the last highest value of the vector
	    double final_charge;
	    if (input_charge >= q_bins[num_bins-1]) {
              final_charge = q_bins[num_bins-1];
            }
	    // if the charge is less than the first bin, we will not measure the charge
	    else if (input_charge <= q_bins[0]) {
	      final_charge = 0;
	    }
	    else {
	      // find in between which bins the charge lies
	      for (int i = 0; i < num_bins - 1; ++i) {
		if (input_charge >= q_bins[i] && input_charge < q_bins[i + 1]) {
		  bin_index = i;
		  break;
		}
	      }
	      
	      final_charge = q_bins[bin_index];
	    }


	    //////////////////////////////////////////////////////////////////////////////////////////
            // new_signal = std::min(new_signal, 1000.0f);

            // Print for debug purposesss
            std::stringstream description;
#ifdef PRINT_CSV_INFO
            description << "\nHit signal: " << hit.getAbsoluteCharge() << "\nConverted to FC (signal'): " << signal;
            description << "\nUsing [" << idx << "]: ";
            yy(description, f);
            description << "\nf(signal') = " << f(signal) << "\nAdding noise: " << noise << "\nVn = " << Vn;
            description << "\nBinned new V: " << binned;
            description << "\nNew charge after the inversion = " << new_signal;
#endif
            hits.emplace_back(global_center.x(),
                              global_center.y(),
                              global_center.z(),
                              index.x(),
                              index.y(),
			      //new_signal,
                              final_charge,
                              signal,
                              description.str());
        }

        if(!hits.size()) {
            continue;
        }

        auto corrected_hits = std::make_shared<SimplifiedHitMessage>(hits, message->getDetector());

        messenger_->dispatchMessage(this, corrected_hits, event);
    }
}
