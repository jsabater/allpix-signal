/**
 * @file
 * @brief Definition of [Calibration] module
 *
 * @copyright Copyright (c) 2017-2022 CERN and the Allpix Squared authors.
 * This software is distributed under the terms of the MIT License, copied verbatim in the file "LICENSE.md".
 * In applying this license, CERN does not waive the privileges and immunities granted to it by virtue of its status as an
 * Intergovernmental Organization or submit itself to any jurisdiction.
 *
 * Contains minimal dummy module to use as a start for the development of your own module
 *
 * Refer to the User's Manual for more details.
 */

#include <optional>
#include <string>

#include "core/config/Configuration.hpp"
#include "core/geometry/GeometryManager.hpp"
#include "core/messenger/Messenger.hpp"
#include "core/module/Event.hpp"
#include "core/module/Module.hpp"

#include "objects/PixelHit.hpp"

#include "CalibrationUtils.hpp"

namespace allpix {
    /**
     * @ingroup SequentialModules
     * @brief SequentialModule to do function
     *
     * More detailed explanation of module
     */
    class CalibrationModule : public SequentialModule {
    public:
        /**
         * @brief Constructor for this unique module
         * @param config Configuration object for this module as retrieved from the steering file
         * @param messenger Pointer to the messenger object to allow binding to messages on the bus
         * @param geo_manager Pointer to the geometry manager, containing the detectors
         */
        CalibrationModule(Configuration& config, Messenger* messenger, GeometryManager* geo_manager);

        /**
         * @brief [Initialise this module]
         */
        void initialize() override;

        /**
         * @brief [Run the function of this module]
         */
        void run(Event* event) override;

    private:
        // General module members
        GeometryManager* geo_manager_;
        Messenger* messenger_;

        double first_bin_;
        std::optional<std::function<double(double)>> bin_function_;
        double bin_step_;
        std::mt19937 random_engine_;
        std::optional<std::normal_distribution<double>> normal_distribution_;
        std::optional<std::uniform_int_distribution<int>> uniform_distribution_;
        std::vector<internal::FittedFunction> functions_;
        std::vector<std::function<double(double)>> inverse_functions_;

        std::tuple<int, const internal::FittedFunction&, const std::function<double(double)>&> get_random_function();
    };
} // namespace allpix
