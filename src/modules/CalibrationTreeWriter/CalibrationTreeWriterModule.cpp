/**
 * @file
 * @brief Implementation of RCE Writer Module
 *
 * @copyright Copyright (c) 2017-2022 CERN and the Allpix Squared authors.
 * This software is distributed under the terms of the MIT License, copied verbatim in the file "LICENSE.md".
 * In applying this license, CERN does not waive the privileges and immunities granted to it by virtue of its status as an
 * Intergovernmental Organization or submit itself to any jurisdiction.
 */

#include "CalibrationTreeWriterModule.hpp"

#include <cassert>
#include <filesystem>
#include <fstream>
#include <stdexcept>

#include <TBranchElement.h>
#include <TClass.h>
#include <TDecompSVD.h>
#include <TDirectory.h>
#include <TMatrixD.h>

//#include "core/geometry/HybridPixelDetectorModel.hpp"
#include "core/utils/log.h"
#include "core/utils/type.h"
#include "core/utils/unit.h"
#include "objects/Object.hpp"
#include "objects/objects.h"

using namespace allpix;

CalibrationTreeWriterModule::CalibrationTreeWriterModule(Configuration& config,
                                                         Messenger* messenger,
                                                         GeometryManager* geo_mgr)
    : SequentialModule(config), messenger_(messenger), geo_mgr_(geo_mgr) {
    // Enable multithreading of this module if multithreading is enabled
    allow_multithreading();

    assert(messenger && "messenger must be non-null");
    assert(geo_mgr && "geo_mgr must be non-null");

    // Bind to PixelHitMessage
    messenger_->bindMulti<SimplifiedHitMessage>(this, MsgFlags::REQUIRED);

    config_.setDefault("file_name", "rce-data.root");
}

void CalibrationTreeWriterModule::initialize() {
    // Open output data file
    std::string path_data = createOutputFile(config_.get<std::string>("file_name"), "root");
    output_file_ = std::make_unique<TFile>(path_data.c_str(), "RECREATE");
    output_file_->cd();

    auto detector_tree = new TTree("Detector", "Detector position information");
    char name[10000];
    Double_t x, y, z; // TODO: shoule be Scalar
    detector_tree->Branch("Name", name);
    detector_tree->Branch("X", &x);
    detector_tree->Branch("Y", &y);
    detector_tree->Branch("Z", &z);

    // Initialize the events tree
    event_tree_ = new TTree("Event", "");
    event_tree_->Branch("TimeStamp", &timestamp_);
    event_tree_->Branch("FrameNumber", &frame_number_);
    event_tree_->Branch("TriggerTime", &trigger_time_);
    event_tree_->Branch("TriggerOffset", &trigger_offset_);
    event_tree_->Branch("TriggerInfo", &trigger_info_);
    event_tree_->Branch("Invalid", &invalid_);

    // For each detector name, initialize an instance of SensorData
    for(const auto& detector : geo_mgr_->getDetectors()) {
        const auto& detector_name = detector->getName();
        auto& sensor = sensors_[detector_name];

        // Register position information for detector
        detector_name.copy(name, 10000);
        auto position = detector->getPosition();
        x = position.X();
        y = position.Y();
        z = position.Z();
        detector_tree->Fill();

        LOG(TRACE) << "Detector " << detector_name;

        TDirectory* detector_dir = output_file_->mkdir(detector_name.c_str());
        detector_dir->cd();

        // Initialize the tree and its branches
        sensor.tree = new TTree("Hits", "");
        sensor.tree->Branch("NHits", &sensor.nhits_);
        sensor.tree->Branch("PixX", sensor.pix_x_, "PixX[NHits]/I");
        sensor.tree->Branch("PixY", sensor.pix_y_, "PixY[NHits]/I");
        sensor.tree->Branch("Value", sensor.value_, "Value[NHits]/D");
        sensor.tree->Branch("ReferenceValue", sensor.reference_value_, "Value[NHits]/D");
        sensor.tree->Branch("Timing", sensor.timing_, "Timing[NHits]/I");
    }
}

void CalibrationTreeWriterModule::run(Event* event) {
    auto pixel_hit_messages = messenger_->fetchMultiMessage<SimplifiedHitMessage>(this, event);

    // fill per-event data
    timestamp_ = 0;
    frame_number_ = event->number;
    trigger_time_ = 0;
    trigger_offset_ = 0;
    trigger_info_ = 0;
    invalid_ = false;
    event_tree_->Fill();
    LOG(TRACE) << "Wrote global event data";

    // reset all per-sensor trees
    for(auto& sensor : sensors_) {
        sensor.second.nhits_ = 0;
    }

    // Loop over the pixel hit messages
    for(const auto& hit_msg : pixel_hit_messages) {
        const auto& detector_name = hit_msg->getDetector()->getName();
        auto& sensor = sensors_[detector_name];

        // Loop over all the hits
        for(const auto& hit : hit_msg->getData()) {
            if(sensor_data::kMaxHits <= sensor.nhits_) {
                LOG(ERROR) << "More than " << sensor_data::kMaxHits << " in detector " << detector_name;
                break;
            }

            auto is_dead = (hit.get_x() - 9) % 17 == 0;

            // LOG(STATUS) << hit.get_y() << " and is " << is_dead;

            if(is_dead) {
                continue;
            }

            // Fill the tree with received messages
            auto i = sensor.nhits_;
            sensor.pix_x_[i] = hit.get_x();                         // NOLINT
            sensor.pix_y_[i] = hit.get_y();                         // NOLINT
            sensor.value_[i] = hit.get_charge();                    // NOLINT
            sensor.reference_value_[i] = hit.get_original_charge(); // NOLINT
            // Assumes that time is correctly digitized
            sensor.timing_[i] = 0; // static_cast<Int_t>(hit.getLocalTime()); // NOLINT
            // This contains no useful information but it expected to be present
            sensor.nhits_ += 1;

            LOG(TRACE) << detector_name << " x=" << hit.get_x() << " y=" << hit.get_y() << " t=" << 0
                       << " charge=" << hit.get_charge();
        }
    }

    // Loop over all the detectors to fill all corresponding sensor trees
    for(auto& [key, sensor] : sensors_) {
        sensor.tree->Fill();
        LOG(TRACE) << "Wrote sensor event data for " << key;
    }
}

void CalibrationTreeWriterModule::finalize() {
    output_file_->Write();
    LOG(TRACE) << "Wrote data to file";
}
