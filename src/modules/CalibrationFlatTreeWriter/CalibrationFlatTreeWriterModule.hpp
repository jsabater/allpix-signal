/**
 * @file
 * @brief Definition of RCE Writer Module
 *
 * @copyright Copyright (c) 2017-2022 CERN and the Allpix Squared authors.
 * This software is distributed under the terms of the MIT License, copied verbatim in the file "LICENSE.md".
 * In applying this license, CERN does not waive the privileges and immunities granted to it by virtue of its status as an
 * Intergovernmental Organization or submit itself to any jurisdiction.
 */

#include <string>
#include <unordered_map>

#include <TFile.h>
#include <TTree.h>

#include "core/config/Configuration.hpp"

#include "core/geometry/GeometryManager.hpp"
#include "core/messenger/Messenger.hpp"

#include "core/module/Event.hpp"
#include "core/module/Module.hpp"
#include "objects/SimplifiedHit.hpp"

namespace allpix {
    /**
     * @ingroup Modules
     * @brief Module to write object data to ROOT trees in RCE format for telescope reconstruction
     *
     * Listens to the PixelHitMessage. On initialization, creates an Event tree and it's respective branches, and
     * a root sub-directory for each detector with the name Plane* where * is the detector number. In each detector
     * sub-directory, it creates a Hits tree. Upon receiving the Pixel Hit messages, it writes the information to
     * the respective trees.
     */
    class CalibrationFlatTreeWriterModule : public SequentialModule {
    public:
        /**
         * @brief Constructor for this unique module
         * @param config Configuration object for this module as retrieved from the steering file
         * @param messenger Pointer to the messenger object to allow binding to messages on the bus
         * @param geo_mgr Pointer to the geometry manager, containing the detectors
         */
        CalibrationFlatTreeWriterModule(Configuration& config, Messenger*, GeometryManager*);
        /**
         * @brief Destructor deletes the internal objects used to build the ROOT Tree
         */
        ~CalibrationFlatTreeWriterModule() override = default;

        /**
         * @brief Opens the file to write the objects to, and initializes the trees
         */
        void initialize() override;

        /**
         * @brief Writes the objects fetched to their specific tree
         */
        void run(Event* event) override;

        /**
         * @brief Write the output file
         */
        void finalize() override;

    private:
        Messenger* messenger_;
        GeometryManager* geo_mgr_;

        // Struct to store tree and information for each detector
        struct SensorData {
            static constexpr int kMaxHits = (1 << 14);
            Int_t nhits_{};
            std::vector<Int_t> layer_{};
            std::vector<Double_t> det_x_{};
            std::vector<Double_t> det_y_{};
            std::vector<Double_t> det_z_{};
            std::vector<Int_t> pix_x_{};
            std::vector<Int_t> pix_y_{};
            std::vector<Double_t> value_{};
            std::vector<Double_t> reference_value_{};
            std::vector<Double_t> xBeam_{};
            std::vector<Double_t> yBeam_{};
            std::vector<Double_t> zBeam_{};
	    std::vector<Double_t> xBeamEnd_{};
	    std::vector<Double_t> yBeamEnd_{};
	    std::vector<Double_t> zBeamEnd_{};
            std::vector<Double_t> eBeam_{};
            std::vector<Int_t> pdgIDBeam_{};
            std::vector<Int_t> terminatingVolume_{};
            std::vector<std::string> terminatingVolumeName_{};

	  //            Double_t xBeam_{};
	  //            Double_t yBeam_{};
	  //            Double_t zBeam_{};

            Double_t QfCtot_{};
            Double_t QfCl1_{};
            Double_t QfCl2_{};
            Double_t QfCl3_{};
            Double_t QfCl4_{};
            Double_t QfCl5_{};
            Double_t QfCl6_{};

            Double_t QfCrtot_{};
            Double_t QfCrl1_{};
            Double_t QfCrl2_{};
            Double_t QfCrl3_{};
            Double_t QfCrl4_{};
            Double_t QfCrl5_{};
            Double_t QfCrl6_{};
        };
        // The map from detector names to the respective sensor_data struct
        SensorData sensor_{};

        // Relevant information for the tree
        ULong64_t frame_number_{};
        // The Event tree
        TTree* tree_; // no unique_ptr, ROOT takes ownership

        std::vector<Double_t> layer_offesets_{};

        // Output data file to write
        std::unique_ptr<TFile> output_file_;
    };
} // namespace allpix
