/**
 * @file
 * @brief Implementation of RCE Writer Module
 *
 * @copyright Copyright (c) 2017-2022 CERN and the Allpix Squared authors.
 * This software is distributed under the terms of the MIT License, copied verbatim in the file "LICENSE.md".
 * In applying this license, CERN does not waive the privileges and immunities granted to it by virtue of its status as an
 * Intergovernmental Organization or submit itself to any jurisdiction.
 */

#include "CalibrationFlatTreeWriterModule.hpp"

#include <cassert>
#include <filesystem>
#include <fstream>
#include <stdexcept>

#include <TBranchElement.h>
#include <TClass.h>
#include <TDecompSVD.h>
#include <TDirectory.h>
#include <TMatrixD.h>

//#include "core/geometry/HybridPixelDetectorModel.hpp"
#include "core/utils/log.h"
#include "core/utils/type.h"
#include "core/utils/unit.h"
#include "objects/Object.hpp"
#include "objects/objects.h"

using namespace allpix;

CalibrationFlatTreeWriterModule::CalibrationFlatTreeWriterModule(Configuration& config,
                                                                 Messenger* messenger,
                                                                 GeometryManager* geo_mgr)
    : SequentialModule(config), messenger_(messenger), geo_mgr_(geo_mgr) {
    // Enable multithreading of this module if multithreading is enabled
    allow_multithreading();
  
    assert(messenger && "messenger must be non-null");
    assert(geo_mgr && "geo_mgr must be non-null");

    // Bind to PixelHitMessage
    messenger_->bindMulti<SimplifiedHitMessage>(this, MsgFlags::REQUIRED);
    messenger_->bindSingle<MCTrackMessage>(this, MsgFlags::REQUIRED);

    config_.setDefault("file_name", "flat-data.root");
}

void CalibrationFlatTreeWriterModule::initialize() {
    // Open output data file
    std::string path_data = createOutputFile(config_.get<std::string>("file_name"), "root");
    output_file_ = std::make_unique<TFile>(path_data.c_str(), "RECREATE");
    output_file_->cd();

    tree_ = new TTree("myTree", "myTree");

    // Initialize the events tree
    tree_->Branch("eventID", &frame_number_);
    tree_->Branch("nHits", &sensor_.nhits_);
    tree_->Branch("layer", &sensor_.layer_);
    tree_->Branch("x", &sensor_.det_x_);
    tree_->Branch("y", &sensor_.det_y_);
    tree_->Branch("z", &sensor_.det_z_);
    tree_->Branch("indexX", &sensor_.pix_x_);
    tree_->Branch("indexY", &sensor_.pix_y_);
    tree_->Branch("QfC_meas", &sensor_.value_);
    tree_->Branch("QfC", &sensor_.reference_value_);
    tree_->Branch("xBeam", &sensor_.xBeam_);
    tree_->Branch("yBeam", &sensor_.yBeam_);
    tree_->Branch("zBeam", &sensor_.zBeam_);
    tree_->Branch("xBeamEnd", &sensor_.xBeamEnd_);
    tree_->Branch("yBeamEnd", &sensor_.yBeamEnd_);
    tree_->Branch("zBeamEnd", &sensor_.zBeamEnd_);
    tree_->Branch("eBeam", &sensor_.eBeam_);
    tree_->Branch("pdgIDBeam", &sensor_.pdgIDBeam_);
    tree_->Branch("terminatingVolume", &sensor_.terminatingVolume_);
    tree_->Branch("terminatingVolumeName", &sensor_.terminatingVolumeName_);

    //    tree_->Branch("xBeam", &sensor_.xBeam_);
    //    tree_->Branch("yBeam", &sensor_.yBeam_);
    //    tree_->Branch("zBeam", &sensor_.zBeam_);

    tree_->Branch("QfCtot_meas", &sensor_.QfCtot_);
    tree_->Branch("QfC_l1_meas", &sensor_.QfCl1_);
    tree_->Branch("QfC_l2_meas", &sensor_.QfCl2_);
    tree_->Branch("QfC_l3_meas", &sensor_.QfCl3_);
    tree_->Branch("QfC_l4_meas", &sensor_.QfCl4_);
    tree_->Branch("QfC_l5_meas", &sensor_.QfCl5_);
    tree_->Branch("QfC_l6_meas", &sensor_.QfCl6_);

    tree_->Branch("QfCtot", &sensor_.QfCrtot_);
    tree_->Branch("QfC_l1", &sensor_.QfCrl1_);
    tree_->Branch("QfC_l2", &sensor_.QfCrl2_);
    tree_->Branch("QfC_l3", &sensor_.QfCrl3_);
    tree_->Branch("QfC_l4", &sensor_.QfCrl4_);
    tree_->Branch("QfC_l5", &sensor_.QfCrl5_);
    tree_->Branch("QfC_l6", &sensor_.QfCrl6_);

    // For each detector name, initialize an instance of SensorData
    std::set<Double_t> offsets{};

    for(const auto& detector : geo_mgr_->getDetectors()) {
        auto offset = detector->getPosition().Z();
        offsets.emplace(offset);
    }

    layer_offesets_.assign(offsets.begin(), offsets.end());
}

Int_t extract_int(const std::string& str) {
    auto* digits = "0123456789";
    auto n = str.find_first_of(digits);

    if(n == std::string::npos) {
        return -1;
    }

    auto m = str.find_first_not_of(digits, n);
    auto number_string = str.substr(n, m != std::string::npos ? m - n : m);

    return std::stoi(number_string);
}

void CalibrationFlatTreeWriterModule::run(Event* event) {

    auto pixel_hit_messages = messenger_->fetchMultiMessage<SimplifiedHitMessage>(this, event);
    auto mc_track_message = messenger_->fetchMessage<MCTrackMessage>(this, event);

    std::unordered_set<const MCTrack*> tracks;

    for(auto& mc_track : mc_track_message->getData()) {
        auto* track_ptr = &mc_track;

        while(track_ptr->getParent() != nullptr) {
            track_ptr = track_ptr->getParent();
        }

        tracks.insert(track_ptr);
    }

    
    for(auto track_ptr : tracks) {
        auto& mc_track = *track_ptr;

        // select the shooting photon
	//        if(mc_track.getParticleID() == 22 && mc_track.getCreationProcessName() == "none") {
        if(mc_track.getCreationProcessName() == "none") {
            auto start_point = mc_track.getStartPoint();
	    auto end_point = mc_track.getEndPoint();
            auto energy = mc_track.getTotalEnergyInitial() / 1000.0f;
            sensor_.xBeam_.emplace_back(start_point.X());
            sensor_.yBeam_.emplace_back(start_point.Y());
            sensor_.zBeam_.emplace_back(start_point.Z());
	    sensor_.xBeamEnd_.emplace_back(end_point.X());
            sensor_.yBeamEnd_.emplace_back(end_point.Y());
            sensor_.zBeamEnd_.emplace_back(end_point.Z());
            sensor_.eBeam_.emplace_back(energy);
            sensor_.pdgIDBeam_.emplace_back(mc_track.getParticleID());
            auto terminating_volume_name = mc_track.getTerminatingVolumeName();
            sensor_.terminatingVolumeName_.emplace_back(terminating_volume_name);
            sensor_.terminatingVolume_.emplace_back(extract_int(terminating_volume_name));
        }
	/*
        if(mc_track.getParticleID() != 22 && mc_track.getCreationProcessName() == "none") {
            auto start_point = mc_track.getStartPoint();
            sensor_.xBeam_ = start_point.X();
            sensor_.yBeam_ = start_point.Y();
            sensor_.zBeam_ = start_point.Z();

	    //  LOG(STATUS) << "sensor_.xBeam_= " << sensor_.xBeam_ << " sensor_.yBeam_= " << sensor_.yBeam_
	    //           << " sensor_.zBeam_= " << sensor_.zBeam_;
        }
	*/
    }

    // fill per-event data
    frame_number_ = event->number;
    LOG(TRACE) << "Wrote global event data";

    // for simplified geometry
    //    std::vector<Double_t> charges_per_layer(layer_offesets_.size(), 0.0f);
    //    std::vector<Double_t> charges_per_layer_ref(layer_offesets_.size(), 0.0f);
    // for detailed geometry
    std::vector<Double_t> charges_per_layer(layer_offesets_.size()/2, 0.0f);
    std::vector<Double_t> charges_per_layer_ref(layer_offesets_.size()/2, 0.0f);

    // Loop over the pixel hit messages
    for(const auto& hit_msg : pixel_hit_messages) {
        const auto& detector = hit_msg->getDetector();
        const auto& detector_name = detector->getName();

        auto layer = std::distance(layer_offesets_.begin(),
                                   std::find(layer_offesets_.begin(), layer_offesets_.end(), detector->getPosition().Z()));

	// since for the FASER detailed geometry the modules in a given layer have an offset w.r.t each other, allpix gives a total of 12 layers
	// to keep it from 1 to 6 we can round with floor
	// fixme : this logic won't work for geometries where the modules are all flat (same z) in a given layer
       	layer = floor(layer/2); // UNCOMMENT THIS FOR THE DETAILED GEOMETRY!

        // Loop over all the hits
        for(const auto& hit : hit_msg->getData()) {
            if(SensorData::kMaxHits <= sensor_.nhits_) {
                LOG(DEBUG) << "More than " << SensorData::kMaxHits << " in detector " << detector_name
                           << " :: " << sensor_.nhits_;
            }

	    // LOG(STATUS) << "detector name = " << detector_name << " layer = " << layer;
	    // LOG(STATUS) << " detector number = " << detector_name[9];
	    //	    char det_num = detector_name[9];
	    // convert the char to an int
	    //	    int det_numi = det_num - '0';
	    //	    if (det_numi == 0 or det_numi == 1)

            auto is_dead = (hit.get_x() - 9) % 17 == 0;

            if(is_dead) {
                continue;
            }

            charges_per_layer[layer] += hit.get_charge();
            charges_per_layer_ref[layer] += hit.get_original_charge();

            // Fill the tree with received messages
            sensor_.layer_.emplace_back(layer + 1);
            sensor_.det_x_.emplace_back(hit.get_global_x());
            sensor_.det_y_.emplace_back(hit.get_global_y());
            sensor_.det_z_.emplace_back(hit.get_global_z());
            sensor_.pix_x_.emplace_back(hit.get_x());
            sensor_.pix_y_.emplace_back(hit.get_y());
            sensor_.value_.emplace_back(hit.get_charge());
            sensor_.reference_value_.emplace_back(hit.get_original_charge());
            // Assumes that time is correctly digitized
            // This contains no useful information but it expected to be present
            sensor_.nhits_ += 1;

            LOG(TRACE) << detector_name << " x=" << hit.get_x() << " y=" << hit.get_y() << " t=" << 0
                       << " charge=" << hit.get_charge();
        }
    }


    sensor_.QfCl1_ = charges_per_layer[0];
    sensor_.QfCl2_ = charges_per_layer[1];
    sensor_.QfCl3_ = charges_per_layer[2];
    sensor_.QfCl4_ = charges_per_layer[3];
    sensor_.QfCl5_ = charges_per_layer[4];
    sensor_.QfCl6_ = charges_per_layer[5];
    sensor_.QfCtot_ = std::accumulate(charges_per_layer.begin(), charges_per_layer.end(), 0.0f);

    sensor_.QfCrl1_ = charges_per_layer_ref[0];
    sensor_.QfCrl2_ = charges_per_layer_ref[1];
    sensor_.QfCrl3_ = charges_per_layer_ref[2];
    sensor_.QfCrl4_ = charges_per_layer_ref[3];
    sensor_.QfCrl5_ = charges_per_layer_ref[4];
    sensor_.QfCrl6_ = charges_per_layer_ref[5];
    sensor_.QfCrtot_ = std::accumulate(charges_per_layer_ref.begin(), charges_per_layer_ref.end(), 0.0f);

    if(sensor_.nhits_) {
        tree_->Fill();
        sensor_.nhits_ = 0;
        sensor_.layer_.clear();
        sensor_.det_x_.clear();
        sensor_.det_y_.clear();
        sensor_.det_z_.clear();
        sensor_.pix_x_.clear();
        sensor_.pix_y_.clear();
        sensor_.value_.clear();
        sensor_.reference_value_.clear();
        sensor_.xBeam_.clear();
        sensor_.yBeam_.clear();
        sensor_.zBeam_.clear();
	sensor_.xBeamEnd_.clear();
        sensor_.yBeamEnd_.clear();
        sensor_.zBeamEnd_.clear();
        sensor_.eBeam_.clear();
        sensor_.pdgIDBeam_.clear();
        sensor_.terminatingVolumeName_.clear();
        sensor_.terminatingVolume_.clear();
        LOG(TRACE) << "Wrote sensor event data for " << frame_number_;

    }
}

void CalibrationFlatTreeWriterModule::finalize() {
    output_file_->Write();
    LOG(TRACE) << "Wrote data to file";
}
