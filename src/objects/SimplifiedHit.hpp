#ifndef ALLPIX_SIMPLIFIED_HIT_H
#define ALLPIX_SIMPLIFIED_HIT_H

#include "Object.hpp"

namespace allpix {
    /**
     * @ingroup Objects
     * @brief Pixel triggered in an event after digitization
     */
    class SimplifiedHit : public Object {
    public:
        /**
         * @brief Construct a digitized pixel hit
         */
        SimplifiedHit(double global_x,
                      double global_y,
                      double global_z,
                      int32_t pixel_x,
                      int32_t pixel_y,
                      double charge,
                      double original_charge,
                      std::string description);
        SimplifiedHit() = default;

        void loadHistory() override;
        void petrifyHistory() override;

        /**
         * @brief Print an ASCII representation of this Object to the given stream
         * @param out Stream to print to
         */
        void print(std::ostream& out) const override;

        Double_t get_global_x() const;
        Double_t get_global_y() const;
        Double_t get_global_z() const;
        Int_t get_x() const;
        Int_t get_y() const;
        Double_t get_charge() const;
        Double_t get_original_charge() const;

        const std::string description_;

    private:
        Double_t g_x_, g_y_, g_z_;
        Int_t pixel_x_;
        Int_t pixel_y_;
        Double_t charge_;
        Double_t original_charge_;
    };

    /**
     * @brief Typedef for message carrying pixel hits
     */
    using SimplifiedHitMessage = Message<SimplifiedHit>;
} // namespace allpix

#endif
