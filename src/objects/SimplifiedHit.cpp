#include "SimplifiedHit.hpp"

using namespace allpix;

SimplifiedHit::SimplifiedHit(double global_x,
                             double global_y,
                             double global_z,
                             Int_t pixel_x,
                             Int_t pixel_y,
                             Double_t charge,
                             Double_t original_charge,
                             std::string description)
    : g_x_(global_x), g_y_(global_y), g_z_(global_z), pixel_x_(pixel_x), pixel_y_(pixel_y), charge_(charge),
      original_charge_(original_charge), description_(std::move(description)) {}

void SimplifiedHit::loadHistory() {}
void SimplifiedHit::petrifyHistory() {}

void SimplifiedHit::print(std::ostream& out) const {
    out << "<unknown object>";
}

Int_t SimplifiedHit::get_x() const {
    return pixel_x_;
}

Int_t SimplifiedHit::get_y() const {
    return pixel_y_;
}

Double_t SimplifiedHit::get_global_x() const {
    return g_x_;
}

Double_t SimplifiedHit::get_global_y() const {
    return g_y_;
}

Double_t SimplifiedHit::get_global_z() const {
    return g_z_;
}

Double_t SimplifiedHit::get_charge() const {
    return charge_;
}

Double_t SimplifiedHit::get_original_charge() const {
    return original_charge_;
}
